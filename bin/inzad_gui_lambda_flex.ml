
[@@@landmark "auto"]

open Stk

let lambda_string lam =
  let b = Buffer.create 256 in
  let ppf = Format.formatter_of_buffer b in
  Printlambda.lambda ppf lam;
  Format.pp_print_flush ppf ();
  Buffer.contents b

let lambda_hash options lam =
  let config =
      Asak.Lambda_hash.{
        should_sort = options.Inzad.Hash.sort_hashes;
        hash_var = options.hash_variables;
        hash_const = options.hash_constants ;
        apply_holes = options.apply_with_holes ;
      }
  in
  let threshold = options.threshold in
  Asak.Lambda_hash.hash_lambda config threshold lam

let lambda_digest options lam =
  Asak.Lambda_hash.top_digest (lambda_hash options lam)

module LambdaSet = Set.Make
  (struct type t = Lambda.lambda let compare = Stdlib.compare end)

let lambdas =
  let f r lam = r := LambdaSet.add lam !r; lam in
  fun lam ->
    let r = ref LambdaSet.empty in
    ignore(Lambda.map (f r) lam);
    LambdaSet.elements !r

let blank = Re.(alt [blank; char '\n'; char '\r'])
let blank_re = Re.compile (Re.rep1 blank)

let lambda_info options lam =
  let str0 = lambda_string lam in
  let lambdas = lambdas lam in
  List.fold_left
    (fun acc lam ->
       let s = lambda_string lam in
       let len = String.length s in
       let re =
         let words = Re.split blank_re s in
         let rec iter acc = function
         | [] -> Re.seq (List.rev acc)
         | h :: q when acc = [] -> iter [Re.str h] q
         | h :: q -> iter (Re.str h :: Re.rep1 blank :: acc) q
         in
         Re.compile (iter [] words)
       in
       let pos =
         try
           let g = Re.exec re str0 in
           let pos = Some (fst (Re.Group.offset g 0)) in
           Inzad.Log.debug (fun m-> m "%S found in %S" s str0);
           pos
         with Not_found ->
             Inzad.Log.err (fun m-> m "%S not found in %S" s str0);
             None
       in
       match lambda_digest options lam with
       | None ->
           Inzad.Log.warn (fun m -> m "No digest for %a"
             Printlambda.lambda lam);
           acc
       | Some dig -> Inzad.Db.DigMap.add dig (lam, pos, len) acc
    )
    Inzad.Db.DigMap.empty lambdas

module T = Texttag.T

let props_base =
  let open Stk.Props in
  let p = empty () in
  set p padding (trbl__ 0);
  set p bg_color_selected Color.dodgerblue ;
  set p fg_color_selected Color.white ;
  p

let props_common =
  let open Stk.Props in
  let p = dup props_base in
  set p fill true ;
  set p bg_color Color.lightgreen ;
  p
let props_differ =
  let open Stk.Props in
  let p = dup props_base in
  set p fill true ;
  set p bg_color Color.lightcoral ;
  p

(*
let add_theme_tags (w:Widget.widget) =
  let module T = Texttag.Theme in
  let theme = w#get_p T.prop in
  let theme = T.get_or_create theme in
  T.set_tag_prop theme tag_common Props.fill true ;
  T.set_tag_prop theme tag_common Props.bg_color Color.lightgreen ;

  T.set_tag_prop theme tag_differ Props.fill true ;
  T.set_tag_prop theme tag_differ Props.bg_color Color.lightcoral ;

  T.set_tag_prop theme tag_digest Props.fg_color Color.white ;
  T.set_tag_prop theme tag_digest Props.bg_color Color.dodgerblue ;
  T.set_tag_prop theme tag_digest Props.fill true
*)
let view_class = "lambdaview"
let () =
  let p = Props.empty() in
  Stk.Theme.add_class ~inherits:[] view_class p

let lambda_flex_class = "lambdaflex"
let () =
 let p = Props.empty() in
  Props.(set p font_desc (Font.font_desc ~size:14 "DejaVu Sans Mono")) ;
  Stk.Theme.add_class ~inherits:["flex"] lambda_flex_class p

module DM = Inzad.Db.DigMap
class view options =
  let label = Text.label () in
  let frame = Frame.frame ~label:label#coerce () in
  let scr = Bin.scrollbox ~pack:frame#set_child () in
  let () = scr#set_vscrollbar_covers_child false in
  let flex : unit Flex.flex = Flex.flex ~class_:lambda_flex_class
    ~wrap:false ~wrap_on_break:true ~collapse_spaces:false
      ~justification:`Start
      ~items_alignment:`Baseline
      ~pack:scr#set_child ()
  in
  object(self)
    inherit Object.o view_class ()
    val mutable info = Inzad.Db.DigMap.empty
    method info = info
    val mutable code = (None : Lambda.lambda option)
    method code = code
    val mutable digest_widgets = DM.empty
    method coerce = frame#coerce
    method flex = flex
    method clear =
      flex#unpack_all ~destroy:true;
      digest_widgets <- DM.empty
    method set_name = label#set_text
    method set_code ?name c =
      Option.iter self#set_name name ;
      code <- c ;
      self#clear ;
      let text =
        match code with
        | None -> "(no code available)"
        | Some lambda ->
            info <- lambda_info options lambda ;
            lambda_string lambda
      in
      let t = Textview.textview () in
      t#insert text ;
      flex#pack t#coerce

    method add_digest_widgets (dig:Digest.t) (widgets:Widget.widget list) =
      let l =
        match DM.find_opt dig digest_widgets with
        | None -> []
        | Some l -> l
      in
      digest_widgets <- DM.add dig (widgets @ l) digest_widgets

    method highlight_digest dig_opt =
      List.iter (fun w -> w#set_selected false) flex#children_widgets;
      let widgets =
        match dig_opt with
        | None -> []
        | Some dig ->
            let l =
              match DM.find_opt dig digest_widgets with
              | None -> []
              | Some l -> l
            in
            (*Stk.Log.warn (fun m -> m "widgets for digest %s: %s"
               (Digest.to_hex dig)
                 (String.concat ", " (List.map (fun w -> w#me) l)));*)
            l
      in
      List.iter (fun w -> w#set_selected true) widgets
  end

let view options = new view options

[@@@ocaml.warning "-27"]

type Format.stag +=
    | Common
    | Differ

let rec lam =
  let open Lambda in
  let open Format in
  fun tagf ppf l ->
    let lam = lam tagf in
    let tag = tagf l in
    Option.iter (pp_open_stag ppf) tag;
    let () =
      match l with
      | Lvar id ->
          Ident.print ppf id
      | Lmutvar id ->
          fprintf ppf "*%a" Ident.print id
      | Lconst cst ->
          Printlambda.structured_constant ppf cst
      | Lapply ap ->
          let lams ppf largs =
            List.iter (fun l -> fprintf ppf "@ %a" lam l) largs in
          fprintf ppf "@[<2>(apply@ %a%a)@]" lam ap.ap_func lams ap.ap_args
      | Lfunction{kind; params; return; body; attr} ->
          let pr_params ppf params =
            match kind with
            | Curried ->
                List.iter (fun (param, k) ->
                   fprintf ppf "@ %a%a"
                     Ident.print param
                     Printlambda.value_kind k) params
            | Tupled ->
                fprintf ppf " (";
                let first = ref true in
                List.iter
                  (fun (param, k) ->
                     if !first then first := false else fprintf ppf ",@ ";
                     Ident.print ppf param;
                     Printlambda.value_kind ppf k)
                  params;
                fprintf ppf ")" in
          fprintf ppf "@[<2>(function%a@ %a)@]" pr_params params
            lam body
      (*| Llet(str, k, id, arg, body) ->
          let kind = function
            Alias -> "a" | Strict -> "" | StrictOpt -> "o" | Variable -> "v"
          in
          fprintf ppf "@[<2>(let@ @[<hv 1>@[<2>%a =%s%a@ %a@]@]@] in@ %a)"
            Ident.print id (kind str) Printlambda.value_kind k lam arg lam body*)
      | Llet(_, k, id, arg, body)
      | Lmutlet(k, id, arg, body) as l ->
      let let_kind = begin function
        | Llet(str,_,_,_,_) ->
           begin match str with
             Alias -> "a" | Strict -> "" | StrictOpt -> "o"
           end
        | Lmutlet _ -> "mut"
        | _ -> assert false
        end
      in
      let rec letbody = function
        | Llet(_, k, id, arg, body)
        | Lmutlet(k, id, arg, body) as l ->
           fprintf ppf "@ @[<2>%a =%s%a@ %a@]"
             Ident.print id (let_kind l) Printlambda.value_kind k lam arg;
           letbody body
        | expr -> expr in
      fprintf ppf "@[<2>(let@ @[<hv 1>(@[<2>%a =%s%a@ %a@]"
        Ident.print id (let_kind l) Printlambda.value_kind k lam arg;
      let expr = letbody body in
      fprintf ppf ")@]@ %a)@]" lam expr

      | Lletrec(id_arg_list, body) ->
          let bindings ppf id_arg_list =
            let spc = ref false in
            List.iter
              (fun (id, l) ->
                 if !spc then fprintf ppf "@ " else spc := true;
                 fprintf ppf "@[<2>%a@ %a@]" Ident.print id lam l)
              id_arg_list in
          fprintf ppf
            "@[<2>(letrec@ (@[<hv 1>%a@])@ %a)@]" bindings id_arg_list lam body
      | Lprim(prim, largs, _) ->
          let lams ppf largs =
            List.iter (fun l -> fprintf ppf "@ %a" lam l) largs in
          fprintf ppf "@[<2>(%a%a)@]" Printlambda.primitive prim lams largs
      | Lswitch(larg, sw, _loc) ->
          let switch ppf sw =
            let spc = ref false in
            List.iter
              (fun (n, l) ->
                 if !spc then fprintf ppf "@ " else spc := true;
                 fprintf ppf "@[<hv 1>case int %i:@ %a@]" n lam l)
              sw.sw_consts;
            List.iter
              (fun (n, l) ->
                 if !spc then fprintf ppf "@ " else spc := true;
                 fprintf ppf "@[<hv 1>case tag %i:@ %a@]" n lam l)
              sw.sw_blocks ;
            begin match sw.sw_failaction with
              | None  -> ()
              | Some l ->
                  if !spc then fprintf ppf "@ " else spc := true;
                  fprintf ppf "@[<hv 1>default:@ %a@]" lam l
            end in
          fprintf ppf
            "@[<1>(%s %a@ @[<v 0>%a@])@]"
            (match sw.sw_failaction with None -> "switch*" | _ -> "switch")
            lam larg switch sw
      | Lstringswitch(arg, cases, default, _) ->
          let switch ppf cases =
            let spc = ref false in
            List.iter
              (fun (s, l) ->
                 if !spc then fprintf ppf "@ " else spc := true;
                 fprintf ppf "@[<hv 1>case \"%s\":@ %a@]" (String.escaped s) lam l)
              cases;
            begin match default with
              | Some default ->
                  if !spc then fprintf ppf "@ " else spc := true;
                  fprintf ppf "@[<hv 1>default:@ %a@]" lam default
              | None -> ()
            end in
          fprintf ppf
            "@[<1>(stringswitch %a@ @[<v 0>%a@])@]" lam arg switch cases
      | Lstaticraise (i, ls)  ->
          let lams ppf largs =
            List.iter (fun l -> fprintf ppf "@ %a" lam l) largs in
          fprintf ppf "@[<2>(exit@ %d%a)@]" i lams ls;
      | Lstaticcatch(lbody, (i, vars), lhandler) ->
          fprintf ppf "@[<2>(catch@ %a@;<1 -1>with (%d%a)@ %a)@]"
            lam lbody i
            (fun ppf vars ->
               List.iter
                 (fun (x, k) -> fprintf ppf " %a%a" Ident.print x
                    Printlambda.value_kind k)
                 vars
            )
            vars
            lam lhandler
      | Ltrywith(lbody, param, lhandler) ->
          fprintf ppf "@[<2>(try@ %a@;<1 -1>with %a@ %a)@]"
            lam lbody Ident.print param lam lhandler
      | Lifthenelse(lcond, lif, lelse) ->
          fprintf ppf "@[<2>(if@ %a@ %a@ %a)@]" lam lcond lam lif lam lelse
      | Lsequence(l1, l2) ->
          fprintf ppf "@[<2>(seq@ %a@ %a)@]" lam l1 (sequence tagf) l2
      | Lwhile(lcond, lbody) ->
          fprintf ppf "@[<2>(while@ %a@ %a)@]" lam lcond lam lbody
      | Lfor(param, lo, hi, dir, body) ->
          fprintf ppf "@[<2>(for %a@ %a@ %s@ %a@ %a)@]"
            Ident.print param lam lo
            (match dir with Upto -> "to" | Downto -> "downto")
            lam hi lam body
      | Lassign(id, expr) ->
          fprintf ppf "@[<2>(assign@ %a@ %a)@]" Ident.print id lam expr
      | Lsend (k, met, obj, largs, _) ->
          let args ppf largs =
            List.iter (fun l -> fprintf ppf "@ %a" lam l) largs in
          let kind =
            if k = Self then "self" else if k = Cached then "cache" else "" in
          fprintf ppf "@[<2>(send%s@ %a@ %a%a)@]" kind lam obj lam met args largs
      | Levent(expr, ev) ->
          let kind =
            match ev.lev_kind with
            | Lev_before -> "before"
            | Lev_after _  -> "after"
            | Lev_function -> "funct-body"
            | Lev_pseudo -> "pseudo"
          in
          (* -dno-locations also hides the placement of debug events;
             this is good for the readability of the resulting output (usually
             the end-user goal when using -dno-locations), as it strongly
             reduces the nesting level of subterms. *)
          if not !Clflags.locations then lam ppf expr
          else begin match ev.lev_loc with
              | Loc_unknown ->
                  fprintf ppf "@[<2>(%s <unknown location>@ %a)@]" kind lam expr
              | Loc_known {scopes; loc} ->
                  fprintf ppf "@[<2>(%s %s %s(%i)%s:%i-%i@ %a)@]" kind
                    (Debuginfo.Scoped_location.string_of_scopes scopes)
                    loc.Location.loc_start.Lexing.pos_fname
                    loc.Location.loc_start.Lexing.pos_lnum
                    (if loc.Location.loc_ghost then "<ghost>" else "")
                    loc.Location.loc_start.Lexing.pos_cnum
                    loc.Location.loc_end.Lexing.pos_cnum
                    lam expr
            end
      | Lifused(id, expr) ->
          fprintf ppf "@[<2>(ifused@ %a@ %a)@]" Ident.print id lam expr
    in
    Option.iter (fun _ -> pp_close_stag ppf ()) tag

and sequence tagf ppf = function
  | Lsequence(l1, l2) ->
      Format.fprintf ppf "%a@ %a"
        (sequence tagf) l1
        (sequence tagf) l2
  | l ->
      lam tagf ppf l
[@@@ocaml.warning "+27"]

let cross_views options (v1:view) (v2:view) =
  let mark_open = function
  | Differ -> "<differ>"
  | Common -> "<common>"
  | _ -> ""
  in
  let mark_close = function
  | Differ -> "</differ>"
  | Common -> "</common>"
  | _ -> ""
  in
  let init_ppf () =
    let sob = Format.make_symbolic_output_buffer () in
    let ppf = Format.formatter_of_symbolic_output_buffer sob in
    Format.pp_set_tags ppf true ;
    Format.(pp_set_formatter_stag_functions ppf
     { mark_open_stag = mark_open ;
       mark_close_stag = mark_close ;
       print_open_stag = (fun _ -> ()) ;
       print_close_stag = (fun _ -> ()) ;
     } );
     (sob, ppf)
  in
  let go v1 v2 =
    match v1#code with
    | None -> ()
    | Some l ->
        (* use the tagf function to register the encountered lambdas'digests,
           to that we can compute the location of each digest when
           we output to the sourceview *)
        let digests = ref [] in
        let tagf lam =
          match lambda_digest options lam with
          | None ->
              Inzad.Log.warn (fun m -> m "No digest for %a"
                 Printlambda.lambda lam);
              None
          | Some dig ->
              Inzad.Log.info (fun m -> m "Digest %S for %a"
                (Digest.to_hex dig) Printlambda.lambda lam);
              digests := dig :: !digests ;
              let info = v2#info in
              match Inzad.Db.DigMap.find_opt dig info with
              | None -> Some Differ
              | Some _ -> Some Common
        in
        v1#clear ;
        let (sob, ppf) = init_ppf () in
        lam tagf ppf l;
        Format.pp_print_flush ppf () ;
        let items = Format.flush_symbolic_output_buffer sob in
        let digests = List.rev !digests in
        let flex : unit Flex.flex = v1#flex in
         let rec iter digests props widgets = function
        | [] -> widgets, [], digests
        | Format.Output_flush :: q -> iter digests props widgets q
        | Format.Output_newline :: q ->
            let w = flex#pack_break ~props:props_base () in
            iter digests props (w :: widgets) q
        | Format.Output_string "<differ>" :: q ->
            (match digests with
             | [] -> failwith "no more digests ??"
             | d :: dq ->
                 let (w, q, digests) = iter dq props_differ [] q in
                 v1#add_digest_widgets d w ;
                 iter digests props (w @ widgets) q
            )
        | Format.Output_string "<common>" :: q ->
            (match digests with
             | [] -> failwith "no more digests ??"
             | d :: dq ->
                 let (w, q, digests) = iter dq props_common [] q in
                 v1#add_digest_widgets d w ;
                 iter digests props (w @ widgets) q
            )
        | Format.Output_string ("</differ>"|"</common>") :: q ->
            (widgets, q, digests)
        | Format.Output_string s :: q ->
            let t = Stk.Text.label ~props () in
            let w = t#coerce in
            flex#pack w ;
            (* set text after packing so that font property is inherited from flex
               and texture for text is computed with it *)
            t#set_text s ;
            iter digests props (w :: widgets) q
        | Format.Output_spaces _n :: q ->
            let w = flex#pack_space ~props () in
            iter digests props (w :: widgets) q
        | Format.Output_indent n :: q ->
            let text = String.make n ' ' in
            let t = Stk.Text.label ~props:props_base () in
            let w = t#coerce in
            flex#pack ~kind:(`Space true) w ;
            t#set_text text ;
            iter digests props (w :: widgets) q
        in
        let _ = iter digests (Stk.Props.empty ()) [] items in
        ()
  in
  go v1 v2;
  go v2 v1;


