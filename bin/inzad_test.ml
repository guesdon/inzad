(** Code of test subcommand. *)

let config = Asak.Lambda_hash.{
  should_sort = false; hash_var = true ;
  hash_const = false ;
  apply_holes = false ;
  }

let test _copts includes _args =
  (*let t = Lwt_list.iter_p
    (fun file ->
       let%lwt _ = Inzad.Hash.hash_file ~includes file in
       Lwt.return_unit
    )
      args
  in*)
  let t =
    let%lwt l = Lwt_list.map_p
      (fun file ->
         let%lwt (modname, tt) = Inzad.Hash.load_typedtree ~includes file in
         let program =
           Translmod.transl_implementation modname
             (tt, Typedtree.Tcoerce_none)
         in
         Printlambda.program Format.std_formatter program ;
         let program =
           let lambda =
             Asak.Lambda_normalization.(normalize_local_variables (inline_all program.code))
           in
           { program with code = lambda }
         in
         Format.(pp_print_newline std_formatter ());
         Format.(pp_print_string std_formatter "*********\nnormalized program:");
         Format.(pp_print_newline std_formatter ());
         Printlambda.program Format.std_formatter program ;
         Format.(pp_print_newline std_formatter ());
         let lambdas =
           Asak.Parse_structure.read_structure_with_loc ~prefix:modname tt
         in
         let lambdas =
           Asak.Lambda_hash.map_snd
             (fun x -> Asak.Lambda_normalization.(normalize_local_variables (inline_all x)))
             lambdas
         in
         Lwt.return lambdas
      ) _args
    in
    Lwt_list.iter_s
      (fun lambdas ->
         Lwt_list.iter_s
           (fun (_,lambda) ->
              let hash = Asak.Lambda_hash.(hash_lambda config (Hard 0) lambda) in
              Format.fprintf Format.std_formatter
                "%a:@." Asak.Lambda_hash.pp_top_fingerprint hash;
              Printlambda.lambda Format.std_formatter lambda;
              Format.(pp_print_newline std_formatter ());
              Lwt.return_unit)
           lambdas
      )
      l
  in
  let open Inzad.Db.Entry in
  (*let filter {weight} = function
    | Inzad.Dist.Infinity -> false
    | Regular n ->
      let w = float weight and n = float n in
      (w -. n) /. w > 0.8
  in*)
  (*
  let t =
    match args with
    | [] | [_] -> failwith "Missing files"
    | db :: file :: _ ->
        let%lwt db = Inzad.Db.load_db db in
        let%lwt db2 = Inzad.Db.load_db file in
        let module M = Map.Make (Inzad.Db.Entry) in
        let map = ref M.empty in
        let f_sim = Inzad.Dist.similarity db in
        let compare_to e1 e2 =
          let sim = f_sim e1.hash e2.hash in
          let keep =
            match sim with
            | `None -> false
            | `Same -> true
            | `Diff d -> d > 0.
          in
          if keep then
            let m = M.update e1
              (function None -> Some [sim,e2] | Some l -> Some ((sim,e2)::l))
                !map
            in
            map := m
          else
            ()
        in
        let entries = Inzad.Db.entries db in
        let entries2 = Inzad.Db.entries db2 in
        let compare_to_set e1 =
          Inzad.Db.Entries.iter (compare_to e1) entries
        in
        Inzad.Db.Entries.iter compare_to_set entries2 ;
        let rec print e limit p = function
        | [] -> ()
        | _ when p >= limit -> ()
        | (d, e2) :: q ->
            Format.(fprintf std_formatter "%s <= %s / %s\n"
             (Inzad.Dist.string_of_similarity d)
                 e.name e2.name);
             print e 3 (p+1) q
        in
        M.iter (fun e l -> print e 3 0
           (List.sort
             (fun (s1,_) (s2,_) -> Inzad.Dist.compare_similarity s1 s2) l)
        )
             !map;
        Lwt.return_unit
  in*)
  `Ok (Lwt_main.run t)
  (*
  `Ok ()*)
