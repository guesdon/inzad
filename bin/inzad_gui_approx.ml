open Stk

[@@@landmark "auto"]

let ks_close = Key.keystate ~mods:Tsdl.Sdl.Kmod.ctrl Tsdl.Sdl.K.w

class weight_box weight =
  let scr = Bin.scrollbox () in
  let () = scr#set_vscrollbar_covers_child false in
  let clist = Clist.clist ~pack:scr#set_child () in
  let col_dig =
    let mk =
      Clist.string_cell
        (fun (fp,_) -> (Asak.Lambda_hash.string_of_fingerprint fp, None))
    in
    let sort_fun (fp1,_) (fp2,_) = Asak.Lambda_hash.compare_fingerprint fp1 fp2 in
    Clist.column ~title:"Fingerprint" ~sort_fun mk
  in
  let col_w =
    let mk =
      let props = Props.empty () in
      Props.(set props halign 0.5);
      Clist.string_cell ~props
        (fun (_,w) -> (string_of_float w, None))
    in
    let sort_fun (_,w1) (_,w2) = Float.compare w1 w2 in
    Clist.column ~title:"Weight" ~sort_fun mk
  in
  let _ = clist#add_column col_dig in
  let _ = clist#add_column col_w in
  object(_self)
    method coerce = scr#coerce
    method set_data set =
      let l = Asak.Lambda_hash.FPSet.elements set in
      let l = List.map
        (fun fp -> (fp, weight fp))
          l
      in
      clist#set_list l
    method clist = clist
  end

class result_box ?(sd=false) title compute =
  let wf_title =
    let label = Text.label ~text:title () in
    Frame.frame ~label:label#coerce ()
  in
  let vbox = Pack.vbox ~pack:wf_title#set_child () in
  let hbox_threshold =
    Pack.hbox ~pack:(vbox#pack ~vexpand:0) ()
  in
  let _ = Text.label ~text:"Threshold:"
    ~pack:(hbox_threshold#pack ~hexpand:0) ()
  in
  let scale_th = Range.range
    ~orientation:Props.Horizontal ~range:(0.,1.)
    ~step:0.05 ~bigstep:0.1
    ~pack:hbox_threshold#pack ()
  in
  let entry_th = Edit.entry
    ~pack:(hbox_threshold#pack) ()
  in
  let _ = scale_th#connect (Object.Prop_changed Range.value)
    (fun ~prev:_ ~now -> entry_th#set_text (string_of_float now))
  in
  let hbox_sd =
    Pack.hbox ~pack:(vbox#pack ~vexpand:0) ()
  in
  let () = hbox_sd#set_visible sd in
  let _ = Text.label ~text:"Standard deviation:"
    ~pack:(hbox_sd#pack ~hexpand:0) ()
  in
  let scale_sd = Range.range
    ~orientation:Props.Horizontal ~range:(0.,1.)
    ~step:0.05 ~bigstep:0.1
    ~pack:hbox_sd#pack ()
  in
  let entry_sd = Edit.entry
    ~pack:(hbox_sd#pack) ()
  in
  let _ = scale_sd#connect (Object.Prop_changed Range.value)
    (fun ~prev:_ ~now -> entry_sd#set_text (string_of_float now))
  in
  let scr_entries = Bin.scrollbox ~pack:vbox#pack () in
  let () = scr_entries#set_vscrollbar_covers_child false in
  let entries = Clist.clist
    ~selection_mode:Props.Sel_browse ~pack:scr_entries#set_child ()
  in
  let col_entry =
    let mk = Clist.string_cell (fun (e,_score,_sd) ->
         (e.Inzad.Db.Entry.name, None))
    in
    Clist.column ~title:"Entry" mk
  in
  let _ = entries#add_column col_entry in
  let col_score =
    let mk =
      let props = Props.empty () in
      Props.(set props halign 0.5);
      Clist.string_cell ~props (fun (_e,score,_sd) ->
         (Printf.sprintf "%.2f%%" (100. *. score), None))
    in
    Clist.column ~title:"Score" mk
  in
  let _ = entries#add_column col_score in
  let col_sd =
    let mk =
      let props = Props.empty () in
      Props.(set props halign 0.5);
      Clist.string_cell ~props (fun (_e,_,sd) ->
         (string_of_float sd, None))
    in
    Clist.column ~title:"Std. dev." mk
  in
  let () = if sd then ignore(entries#add_column col_sd) in
  object(self)
    method coerce = wf_title#coerce
    method entries = entries
    initializer
      let _ =
        let threshold = ref 1. in
        let std_dev = ref None in
        let timeout = Lwt_timeout.create 1
          (fun () ->
             let l = compute ?sd:!std_dev !threshold in
             Lwt.async (fun () -> entries#set_list l; Lwt.return_unit)
          )
        in
        let _ = entry_th#connect (Object.Prop_changed Props.text)
          (fun ~prev:_ ~now ->
             try
               threshold := float_of_string now ;
               scale_th#set_value !threshold ;
               Lwt_timeout.start timeout
             with _ -> ()
          )
        in
        if sd then
          (let _ = entry_sd#connect (Object.Prop_changed Props.text)
            (fun ~prev:_ ~now ->
               try
                 let v = float_of_string now in
                 std_dev := Some v;
                 scale_sd#set_value v ;
                 Lwt_timeout.start timeout
             with _ -> ()
            )
          in
          ());
        self#coerce#connect Widget.Destroy
          (fun () -> Lwt_timeout.stop timeout; false);
      in
      scale_th#set_value 0.7 ;
      scale_sd#set_value 0.05 ;
      ()
  end


let win options db funs (e1:Inzad.Db.Entry.t) =
  let w = App.create_window ~resizable:true
    ~w:700 ~h:600 e1.name
  in
  Wkey.add w#coerce ks_close (fun () -> w#close);
(*  Inzad_gui_lambda_flex.add_theme_tags w#coerce;*)
  let mut_incs =
    let l = Inzad.Find.entries_sharing_fp_with db e1 in
    List.map (fun e2 -> (e2, funs.Inzad.Inclusion.mutual_inclusion e1 e2)) l
  in
  let vbox = Pack.vbox ~pack:w#set_child () in
  let _title =
    let props = Props.empty () in
    Props.set_font_size props 20 ;
    Props.(set props bold true);
    let l = Text.label ~props ~text:e1.name ~pack:(vbox#pack ~vexpand:0) () in
    l#set_halign 0.5 ;
    l
  in
  let vpaned = Pack.vpaned ~pack:vbox#pack () in
  let hpaned_results = Pack.hpaned ~pack:vpaned#pack () in
  let res_mutual =
    let compute ?(sd=0.1) th =
      let l = List.fold_left
        (fun acc (e2, mi) ->
           let a = Inzad.Inclusion.avg mi in
           let s = Inzad.Inclusion.standard_deviation mi in
           if a >= th && s <= sd then
             let s = if s < 0.00001 then 0. else s in
             (e2, a, s) :: acc
           else
             acc)
          [] mut_incs
      in
      List.sort (fun (_,a1,_) (_,a2,_) -> compare a2 a1) l
    in
    new result_box ~sd:true "Similarity" compute
  in
  hpaned_results#pack res_mutual#coerce;
  let res_includes =
    let compute ?sd:_ th =
      let l = List.fold_left
        (fun acc (e2, mi) ->
           let v = mi.Inzad.Inclusion.b_in_a in
           if v >= th then (e2, v, 0.) :: acc else acc)
          [] mut_incs
      in
      List.sort (fun (_,a1,_) (_,a2,_) -> compare a2 a1) l
    in
    new result_box "Includes" compute
  in
  hpaned_results#pack res_includes#coerce ;

  let res_included =
    let compute ?sd:_ th =
      let l = List.fold_left
        (fun acc (e2, mi) ->
           let v = mi.Inzad.Inclusion.a_in_b in
           if v >= th then (e2, v, 0.) :: acc else acc)
          [] mut_incs
      in
      List.sort (fun (_,a1,_) (_,a2,_) -> compare a2 a1) l
    in
    new result_box "Is included in" compute
  in
  hpaned_results#pack res_included#coerce ;
  let hpaned_codes = Pack.hpaned ~pack:vpaned#pack () in
  let l1 = Inzad_gui_lambda_flex.view options in
  hpaned_codes#pack l1#coerce ;
  l1#set_code ~name:e1.name e1.code;
  let l2 = Inzad_gui_lambda_flex.view options in
  hpaned_codes#pack l2#coerce ;

  let frame_details = Frame.frame
    ~label:(Text.label ~text:"Details" ())#coerce
      ~pack:vpaned#pack ()
  in
  let vbox_details = Pack.vbox ~pack:frame_details#set_child () in
  let f_contained  () =
    let hb = Pack.hbox ~pack:(vbox_details#pack ~vexpand:0) () in
    hb#set_inter_padding 2;
    let text e1 e2 score = Printf.sprintf "%s contains %.2f%% of %s"
      e2.Inzad.Db.Entry.name (100. *. score) e1.Inzad.Db.Entry.name
    in
    let l = Text.label ~pack:(hb#pack ~hexpand:0) () in
    fun e1 e2 score ->
      l#set_text (text e1 e2 score)
  in
  let set_score1 = f_contained () in
  let set_score2 = f_contained () in
  let hpaned_weights = Pack.hpaned ~pack:vbox_details#pack () in

  let l_common = Text.label
    ~text:"Fingerprints in common: "
      ()
  in
  let f_common = Frame.frame ~label:l_common#coerce ~pack:hpaned_weights#pack () in
  let common_weights = new weight_box
    (fun fp -> funs.Inzad.Inclusion.weight fp)
  in
  f_common#set_child common_weights#coerce ;
  let hash_box () =
    let box_label = Pack.hbox () in
    let _l = Text.label
      (*~text:"Fingerprints only in "*)
        ~pack:(box_label#pack ~hexpand:0) ()
    in
    let l = Text.label ~pack:box_label#pack () in
    let frame = Frame.frame ~label:box_label#coerce () in
    let b = new weight_box (fun fp -> funs.Inzad.Inclusion.weight fp) in
    frame#set_child b#coerce ;
    (l#set_text, b, frame)
  in
  let (set_text_e1, box_weight_e1, frame_e1) = hash_box () in
  let (set_text_e2, box_weight_e2, frame_e2) = hash_box () in
  let vpaned_uncommon = Pack.vpaned ~pack:hpaned_weights#pack () in
  vpaned_uncommon#pack frame_e1#coerce ;
  vpaned_uncommon#pack frame_e2#coerce ;

  vpaned#set_handle_positions [ Some (`Percent 33.3); Some (`Percent 50.)];
  hpaned_results#set_handle_positions [ Some (`Percent 33.3); Some (`Percent 50.)];
  hpaned_codes#set_handle_positions [ Some (`Percent 50.) ];
  hpaned_weights#set_handle_positions [ Some (`Percent 50.) ];
  vpaned_uncommon#set_handle_positions [ Some (`Percent 50.) ];
  (*Log.warn (fun m -> m "%a" Widget.pp_widget_tree w#wtree);*)
(*
  let fnorm =
    match sim.Inzad.Dist.compute db sim.cache e1 e1 with
    | None -> assert false
    | Some d ->
        Log.warn (fun m -> m "%s: max similarity = %f" e1.Inzad.Db.Entry.name d);
        fun w -> w /. d
  in
*)
  let display_e2 e2 =
    Log.debug (fun m -> m "selected entry %s" e2.Inzad.Db.Entry.name);
    l2#set_code ~name:e2.name e2.Inzad.Db.Entry.code ;
    Inzad_gui_lambda_flex.cross_views options l1 l2;
    let mi = funs.Inzad.Inclusion.mutual_inclusion e1 e2 in
    set_score1 e1 e2 mi.a_in_b;
    set_score2 e2 e1 mi.b_in_a;
    let hash1 = e1.Inzad.Db.Entry.hash in
    let hash2 = e2.Inzad.Db.Entry.hash in
    let common = Asak.Lambda_hash.FPSet.inter hash1 hash2 in
    common_weights#set_data common ;
    set_text_e1 e1.Inzad.Db.Entry.name ;
    box_weight_e1#set_data (Asak.Lambda_hash.FPSet.diff hash1 common) ;
    set_text_e2 e2.Inzad.Db.Entry.name ;
    box_weight_e2#set_data (Asak.Lambda_hash.FPSet.diff hash2 common) ;
  in
  ignore(res_mutual#entries#connect_row_selected
   (fun _ (e2,_,_) ->
      res_includes#entries#unselect_all ;
      res_included#entries#unselect_all ;
      display_e2 e2));
  ignore(res_includes#entries#connect_row_selected
   (fun _ (e2,_,_) ->
      res_mutual#entries#unselect_all ;
      res_included#entries#unselect_all ;
      display_e2 e2));
  ignore(res_included#entries#connect_row_selected
   (fun _ (e2,_,_) ->
      res_mutual#entries#unselect_all ;
      res_includes#entries#unselect_all ;
      display_e2 e2));
(*      let w factor set =
        let sum = Asak.Lambda_hash.FPSet.fold
          (fun fp acc -> Inzad.(Find.dist.Dist.weight db fp) +. acc)
          set 0.
        in
        fnorm (sum *. factor)
      in
      l_weight_common#set_text
        (string_of_float (w Inzad.(Find.dist.Dist.common_factor) common));
      l_weight_uncommon#set_text
        (string_of_float (w Inzad.(Find.dist.Dist.diff_factor) uncommon));
*)
  ignore(res_mutual#entries#connect_row_unselected (fun _ _ -> l2#clear));
  ignore(res_includes#entries#connect_row_unselected (fun _ _ -> l2#clear));
  ignore(res_included#entries#connect_row_unselected (fun _ _ -> l2#clear));
  let connect_weight_clist_select wb other_lists =
    let f _ (fp,_) =
      Log.debug (fun m -> m "selected fingerprint %a" Asak.Lambda_hash.pp_fingerprint fp);
      let dig =
        match fp with
        | Holed (_,subfp) ->
            let dig = Asak.Lambda_hash.fingerprint_digest fp in
            let subdig = Asak.Lambda_hash.fingerprint_digest subfp in
            Log.warn (fun m -> m "selecting Holed dig=%s, subdig=%s"
               (Digest.to_hex dig) (Digest.to_hex subdig));
            subdig
        | fp -> Asak.Lambda_hash.fingerprint_digest fp
      in
      List.iter (fun l -> l#clist#unselect_all) other_lists ;
      l1#highlight_digest (Some dig) ;
      l2#highlight_digest (Some dig) ;
    in
    ignore(wb#clist#connect_row_selected f);
    let f _ _ =
      l1#highlight_digest None;
      l2#highlight_digest None
    in
    ignore(wb#clist#connect_row_unselected f)
  in
  let lists =  [ common_weights ; box_weight_e1 ; box_weight_e2 ] in
  List.iter (fun l ->
       connect_weight_clist_select l
         (List.filter (fun l2 -> l2#coerce#id <> l#coerce#id) lists))
    lists;

