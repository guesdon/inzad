

(* Prevent format tags registered in OCaml's Misc.Color to override
  our styling *)
let () = Misc.Color.(setup (Some Never))

let help _copts man_format cmds topic = match topic with
| None -> `Help (`Pager, None) (* help about the program. *)
| Some topic ->
    let topics = cmds in
    let conv, _ = Cmdliner.Arg.enum (List.rev_map (fun s -> (s, s)) topics) in
    match conv topic with
    | `Error e -> `Error (false, e)
    | `Ok t when t = "commands" -> List.iter print_endline topics; `Ok ()
    | `Ok t when List.mem t cmds -> `Help (man_format, Some t)
    | `Ok _t ->
        let page = (topic, 7, "", "", ""), [`S topic; `P "Say something";] in
        `Ok (Cmdliner.Manpage.print man_format Format.std_formatter page)

let db_show copts args =
  Inzad.Options.install_log_reporter copts ;
  let t = Lwt_list.iter_s (Inzad.Db.show copts) args in
  `Ok (Lwt_main.run t)

let db_build copts includes add hash_options keep_code modules cmti first args =
  Inzad.Options.install_log_reporter copts ;
  let t = Inzad.Db.build copts ~includes hash_options
    ~add ~keep_code ~modules ~cmti ~dst:first args
  in
  `Ok (Lwt_main.run t)

let cluster copts format first =
  Inzad.Options.install_log_reporter copts ;
  let t =
    let%lwt db = Inzad.Db.load_db first in
    let cluster = Inzad.Cluster.clusterize db in
    let () =
      match format with
      | `Text -> Inzad.Cluster.pp_cluster Format.std_formatter cluster
      | `Dot -> Inzad.Cluster.pp_dot Format.std_formatter cluster
    in
    Lwt.return_unit
  in
  `Ok (Lwt_main.run t)

let find copts hash_options sharing dist mutinc cmti includes dbs args =
  Inzad.Options.install_log_reporter copts ;
  let approx =
    match sharing, dist, mutinc with
    | None, None, None -> None
    | Some _, Some _, _
    | _, Some _, Some _
    | Some _, _, Some _ ->
        failwith "Choose between 'sharing' or 'dist' or 'mutual include' approximation"
    | Some th, None, None -> Some (Inzad.Find.Sharing th)
    | None, Some th, None -> Some (Inzad.Find.Includes th)
    | None, None, Some (th,sd) -> Some (Inzad.Find.Mutual_include (th, sd))
  in
  let t =
    let%lwt results =
      Inzad.Find.find copts hash_options ?approx ~cmti ~includes ~dbs args
    in
    List.iter Inzad.Find.print_find_result results;
    Lwt.return_unit
  in
  `Ok (Lwt_main.run t)

let test copts includes args =
  Inzad.Options.install_log_reporter copts ;
  Inzad_test.test copts includes args

open Cmdliner
(* Doc: https://erratique.ch/software/cmdliner/doc/Cmdliner *)


let help_secs = [
 `S Manpage.s_common_options;
 `P "These options are common to all commands.";
 `S "MORE HELP";
 `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";(*`Noblank;*)
(*
 `P "Use `$(mname) help patterns' for help on patch matching."; `Noblank;
 `P "Use `$(mname) help environment' for help on environment variables.";
*)
 `S Manpage.s_bugs; `P "Check bug reports at https://gitlab.inria.fr/guesdon/inzad";
  ]

(* Options common to all commands *)


let db_show_cmd =
  let doc = "show database(s)" in
  let man =
    [`S Manpage.s_description;
     `P "show inzad database(s)" ;
     `Blocks help_secs; ]
  in
  let args =
    let doc = "Inzad database(s)" in
    Arg.(value & pos_all string [] & info [] ~docv:"ARGS" ~doc)
  in
  let info = Cmd.info "show" ~doc ~man in
  Cmd.v info
    Term.(ret (const db_show $ Inzad.Options.copts_t $ args))

let db_build_cmd =
  let doc = "build database" in
  let man =
    [`S Manpage.s_description;
     `P "build inzad database" ;
     `Blocks help_secs; ]
  in

  let add =
    let doc = "add hashes to the given database" in
    Arg.(value & flag & info ["a"; "add"] ~doc)
  in
  let first =
    let doc = "Database file to build" in
    Arg.(required & pos 0 (some string) None & info [] ~docv:"DB" ~doc)
  in
  let args =
    let doc = "Files/directories" in
    Arg.(value & pos_right 0 string [] & info [] ~docv:"ARGS" ~doc)
  in
  let info = Cmd.info "build" ~doc ~man in
  Cmd.v info
    Term.(ret (const db_build $ Inzad.Options.copts_t $
      Inzad.Options.includes $ add
        $ Inzad.Options.hash_opts_t
        $ Inzad.Options.keep_code
        $ Inzad.Options.modules
        $ Inzad.Options.cmti $ first $ args))

let db_cmd =
  let doc = "management of inzad databases" in
  let man =
    [`S Manpage.s_description;
     `P "Manages inzad databases" ;
     `Blocks help_secs; ]
  in
  let subcmds = [ db_build_cmd ; db_show_cmd ] in
  let info = Cmd.info "db" ~doc ~man in
  Cmd.group info subcmds

let cluster_cmd =
  let doc = "partitioning functions according to dissimilarities" in
  let db =
    Arg.(required & pos 0 (some file) None & info [] ~docv:"DBFILE")
  in
  let format =
    let docv = "FORMAT" in
    let c =
      let parser = function
      | "text" -> Ok `Text
      | "dot" -> Ok `Dot
      | s -> Error (`Msg (Printf.sprintf "Unknown format %S" s))
      in
      let printer fmt f = Format.pp_print_string fmt
        (match f with `Text -> "text" | `Dot -> "dot")
      in
      Arg.conv ~docv (parser, printer)
    in
    Arg.(value & opt c `Text & info ["format"] ~docv)
  in
  let info = Cmd.info "cluster" ~doc in
  Cmd.v info
    Term.(ret (const cluster $ Inzad.Options.copts_t $ format $ db))

let find_cmd =
  let doc = "looking for code similarities" in
  let args =
    let doc = "Files/directories" in
    Arg.(value & pos_all string [] & info [] ~docv:"ARGS" ~doc)
  in
  let sharing =
    let docv = "N[%]" in
    let c =
      let parser s =
        try Ok (Inzad.Hash.threshold_of_string s)
        with e -> Error (`Msg (Printexc.to_string e))
      in
      let printer f t =
        Format.pp_print_string f (Inzad.Hash.string_of_threshold t)
      in
      Arg.conv ~docv (parser, printer)
    in
    let doc = "find functions sharing at least N hashes or N% of the hashes weight" in
    Arg.(value & opt (some c) None & info ["sharing"] ~doc ~docv)
  in
  let dist =
    let docv = "0.00..1.00" in
    let doc = "find functions similar by the given threshold" in
    Arg.(value & opt (some float) None & info ["dist"] ~doc ~docv)
  in
  let mutinc =
    let docv = "0.00..1.00,0..." in
    let doc = "find mutually including functions by the given threshold and standard deviation" in
    Arg.(value & opt (some (pair float float)) None & info ["mutinc"] ~doc ~docv)
  in
  let info = Cmd.info "find" ~doc in
  Cmd.v info
    Term.(ret (const find $ Inzad.Options.copts_t
      $ Inzad.Options.hash_opts_t
        $ sharing $ dist $ mutinc
        $ Inzad.Options.cmti
        $ Inzad.Options.includes
        $ Inzad.Options.dbs $ args))

let test_cmd =
  let args =
    let doc = "cmt files" in
    Arg.(non_empty & pos_all file [] & info [] ~docv:"CMTFILES" ~doc)
  in
  let info = Cmd.info "test" in
  Cmd.v info Term.(ret (const test
    $ Inzad.Options.copts_t
    $ Inzad.Options.includes $ args))

let help_cmd =
  let doc = "display help about command and subcommands" in
  let cmd =
    let doc = "The command to get help on. `commands' lists the commands." in
    Arg.(value & pos 0 (some string) None & info [] ~docv:"COMMAND" ~doc)
  in
  let man =
    [`S Manpage.s_description;
     `P "Prints help" ;
     `Blocks help_secs; ]
  in
  let info = Cmd.info "help" ~doc ~man in
  Cmd.v info
    Term.(ret (const help
      $ Inzad.Options.copts_t $ Arg.man_format $ Term.choice_names $cmd))

let default_cmd =
  Term.(ret (const (fun _ -> `Help (`Pager, None)) $ Inzad.Options.copts_t))

let cmds = [db_cmd; cluster_cmd ; find_cmd ; test_cmd; help_cmd]

let main_cmd =
  let doc = "A tool to look for ocaml code similarities" in
  let sdocs = Manpage.s_common_options in
  let man = help_secs in
  let info = Cmd.info ~version:"0.1" ~doc ~sdocs ~man "inzad" in
  Cmd.group info ~default:default_cmd cmds

let () =
  try exit (Cmd.eval ~catch:false main_cmd)
  with
  | Cmi_format.Error e ->
      Inzad.Log.err (fun m -> m "%a" Cmi_format.report_error e);
      exit 1
  | e ->
      Inzad.Log.debug (fun m -> m "%s" (Printexc.get_backtrace()));
      let msg =
        match e with
        | Failure msg
        | Sys_error msg -> msg
        | _ -> Printexc.to_string e
      in
      Inzad.Log.err (fun m -> m "%s" msg);
      exit 1

