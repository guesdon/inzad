open Stk
open Stk.Misc

[@@@landmark "auto"]

let _ = Tsdl.Sdl.(init Init.(video+events))
let () = Lwt_main.run(App.init ())

let ks_incr_log = Key.keystate ~mods:Tsdl.Sdl.Kmod.(ctrl+shift) Tsdl.Sdl.K.l
let ks_decr_log = Key.keystate ~mods:Tsdl.Sdl.Kmod.ctrl Tsdl.Sdl.K.l
let ks_quit = Key.keystate ~mods:Tsdl.Sdl.Kmod.ctrl Tsdl.Sdl.K.q


class results_box () =
  let paned = Pack.hpaned () in
  let scr_entries = Bin.scrollbox ~pack:paned#pack () in
  let () = scr_entries#set_vscrollbar_covers_child false in
  let entries = Clist.clist
    ~selection_mode:Props.Sel_browse ~pack:scr_entries#set_child ()
  in
  let scr_same_entries = Bin.scrollbox ~pack:paned#pack () in
  let () = scr_same_entries#set_vscrollbar_covers_child false in
  let same_entries = Clist.clist
    ~selection_mode:Props.Sel_single ~pack:scr_same_entries#set_child ()
  in

(*  let approx = Clist.clist
    ~selection_mode:Props.Sel_single ~pack:paned#pack ()
  in
*)
  object(_self)
    method coerce = paned#coerce

    method entries = entries
    method same_entries = same_entries
    (*method approx = approx*)

    method fill (results:Inzad.Find.find_result list) =
      entries#set_list results;
      same_entries#set_list [];
      (*approx#set_list [] ;*)

    initializer
      let col_entry =
        let mk = Clist.string_cell (fun r ->
             (r.Inzad.Find.e.Inzad.Db.Entry.name, None))
        in
        Clist.column ~title:"Entry" mk
      in
      ignore(entries#add_column col_entry);
      let col_same =
        let mk = Clist.int_cell (fun r ->
             (List.length r.Inzad.Find.same, None))
        in
        Clist.column ~title:"Same" mk
      in
      ignore(entries#add_column col_same);

      let col_same_entries =
        let mk = Clist.string_cell (fun e ->
             (e.Inzad.Db.Entry.name, None))
        in
        Clist.column ~title:"Same as" mk
      in
      ignore(same_entries#add_column col_same_entries);

      ignore(entries#connect_row_selected
        (fun _ r ->
          Inzad.Log.debug
            (fun m -> m "Entry %S selected" r.e.name);
          (match r.e.code with
           | None -> ()
           | Some l ->
               Inzad.Log.info
                 (fun m -> m "%s Lamba code:@.%a" r.e.name Printlambda.lambda l)
          );
          same_entries#set_list r.Inzad.Find.same))

  end

type gui =
  { window: Window.window ;
    results : results_box ;
  }
let mk_gui () =
  let win = App.create_window
    ~flags:Tsdl.Sdl.Window.resizable
      ~w: 800 ~h: 600 "Inzad"
  in
  let vbox0 = Pack.vbox ~pack:win#set_child () in
  let menubar = Menu.menubar ~pack:vbox0#pack () in
  let (mi_file, _mi_file_lab) =
    Menu.label_menuitem ~text:"File"
    ~pack:menubar#add_item ()
  in
  let menu_file = Menu.menu () in

  let (mi_quit, _) = Menu.label_menuitem
    ~text: "Quit"
    ~shortcut:ks_quit
    ~pack:menu_file#add_item ()
  in
  let _id = mi_quit#connect Widget.Activated App.quit in
  let () = mi_file#set_menu menu_file in

  let (mi_view, _mi_view_lab) =
    Menu.label_menuitem ~text:"View"
    ~pack:menubar#add_item ()
  in
  let menu_view = Menu.menu ~pack:mi_view#set_menu () in
  let (mi_incr_log, _) = Menu.label_menuitem
    ~text:"Increase log view size"
    ~shortcut:ks_incr_log
    ~pack:menu_view#add_item ()
  in
  let (mi_decr_log, _) = Menu.label_menuitem
    ~text:"Decrease log view size"
      ~shortcut:ks_decr_log
      ~pack:menu_view#add_item ()
  in
  Wkey.add win#coerce ks_quit (fun () -> mi_quit#activate);
  Wkey.add win#coerce ks_incr_log (fun () -> mi_incr_log#activate);
  Wkey.add win#coerce ks_decr_log (fun () -> mi_decr_log#activate);

  let vpaned = Pack.vpaned ~pack:vbox0#pack () in

  let change_log_size incr () =
    let p = match vpaned#handle_positions with
      | [Some (`Percent p)] -> p
      | [Some (`Absolute n)] ->
          let h = (vpaned#geometry).G.h in
          ((float n) /. (float h)) *. 100.
      | _ -> 100.
    in
    let p2 = max 20. (min 100. (p +. incr)) in
    Stk.Log.debug (fun m -> m "vpaned#set_handle_positions `Percent %.2f => %.2f" p p2);
    vpaned#set_handle_positions [Some (`Percent p2)]
  in
  let _id = mi_incr_log#connect Widget.Activated (change_log_size (-10.)) in
  let _id = mi_decr_log#connect Widget.Activated (change_log_size 10.) in

  let box_results = Pack.hbox ~pack:vpaned#pack () in
  let results_box = new results_box () in
  box_results#pack results_box#coerce ;

  let logscr = Stk.Bin.scrollbox ~pack:vpaned#pack () in
  let () = logscr#set_vscrollbar_covers_child false in
  let logbox = Stk.Textlog.textlog ~pack:logscr#set_child () in
  logbox#set_theme "terminal" ;
  Logs.set_reporter (Stk.Textlog.reporter logbox);
  Inzad.Log.debug (fun m -> m "Main window created");
  { window = win ;
    results = results_box;
  }

let gui_find_dist options gui db funs () =
  match gui.results#entries#selection_data with
  | [] -> ()
  | _::_::_ -> ()
  | [r] -> Inzad_gui_approx.win options db funs r.e

let gui_find_dist_from_value_name options gui db funs name =
   match Array.find_opt
    (fun r -> r.Inzad.Find.e.Inzad.Db.Entry.name = name) gui.results#entries#data
  with
  | None -> Log.warn (fun m -> m "Entry %S not found" name)
  | Some r -> Inzad_gui_approx.win options db funs r.e

let app_cmd copts hash_options cmti includes dbs value_name src =
  let stk_log_levels = List.map
    (fun src -> (src, Logs.Src.level src))
    (Stk.Log.src_list ())
  in
  Logs.set_level copts.Inzad.Options.log_level;
  List.iter (fun (src, level) -> Logs.Src.set_level src level) stk_log_levels ;

  let f () =
    let gui = mk_gui () in
    let t () =
      (*let%lwt (_db_src, _db) = load_codes hash_options ~cmti ~includes ~dbs src in*)
      Inzad.Log.app (fun m -> m "collecting source codes");
      let%lwt db_src = Inzad.Db.collect_to_db hash_options
        ~keep_code:true ~cmti ~includes src
      in
      Inzad.Log.app (fun m -> m "merging with code databases");
      let%lwt db = Inzad.Find.add_src_to_dbs hash_options dbs db_src in
      Inzad.Log.app (fun m -> m "looking of exact matches");
      let funs = Inzad.(Inclusion.funs db Weight.weight2) in
      let%lwt results = Lwt_list.map_s (Inzad.Find.find_for_entry db funs)
        Inzad.Db.(Entries.elements (entries db_src))
      in
      (*Inzad.Find.find copts hash_options ~keep_code:true ~approx ~cmti ~includes ~dbs src*)
      Inzad.Log.app (fun m -> m "%d results computed" (List.length results));
      gui.results#fill results;

      ignore(gui.results#entries#connect Widget.Clicked
       (fun ev ->
          let event = ev.Widget.event in
          if Tsdl.Sdl.Event.(get event mouse_button_clicks) = 2 then
            (
             gui_find_dist hash_options gui db funs ();
             true
            )
          else
            false
       ));
      Option.iter
        (gui_find_dist_from_value_name hash_options gui db funs)
        value_name ;
      Lwt.return_unit
    in
    Lwt.join [ Stk.App.run () ; t () ]
  in
  Lwt_main.run (f ());
  `Ok ()

open Cmdliner

let main_cmd =
  let doc = "A tool to look for ocaml code similarities" in
  let sdocs = Manpage.s_common_options in
  let info = Cmd.info ~version:"0.1" ~doc ~sdocs "inzad-gui" in
  let value_name =
    let doc = "open window for the given value name" in
    Arg.(value & opt (some string) None & info ["value"] ~docv:"VALUE" ~doc)
  in
  let args =
    let doc = "Files/directories" in
    Arg.(value & pos_all string [] & info [] ~docv:"ARGS" ~doc)
  in
  let term = Term.(ret (const app_cmd
      $ Inzad.Options.copts_t
        $ Inzad.Options.hash_opts_t
        $ Inzad.Options.cmti
        $ Inzad.Options.includes
        $ Inzad.Options.dbs
        $ value_name
        $ args))
  in
  Cmd.v info term

let () =
  try exit (Cmd.eval ~catch:false main_cmd)
  with
  | Cmi_format.Error e ->
      Inzad.Log.err (fun m -> m "%a" Cmi_format.report_error e);
      exit 1
  | e ->
      let bt = Printexc.get_backtrace() in
      Inzad.Log.debug (fun m -> m "%s" bt);
      let msg =
        match e with
        | Failure msg
        | Sys_error msg -> msg
        | _ -> Printexc.to_string e
      in
      prerr_endline bt;
      prerr_endline msg;
      exit 1

