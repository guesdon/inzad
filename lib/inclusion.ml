(** *)
(*
type dist = Asak.Clustering.Distance.t =
  | Regular of int | Infinity

let string_of_dist = function
  | Infinity -> "inf"
  | Regular n -> string_of_int n

let dissimilarity fp1 fp2 =
  let inter = Db.FPSet.inter fp1 fp2 in
  if Db.FPSet.is_empty inter then
    Infinity
  else
    let union = Db.FPSet.union fp1 fp2 in
    let symdiff = Db.FPSet.diff union inter in
    Regular (Db.FPSet.fold
     (fun h acc ->
        match h with
        | W (w,_,_digest) -> acc + w
        | Global_field _ -> acc)
       symdiff 0)

type similarity = float option
let string_of_similarity = function
| None -> "<>"
| Some d -> string_of_float d

let compare_similarity : similarity -> similarity -> int =
  fun a b -> match (a, b) with
    | Some d1, Some d2 -> Float.compare d1 d2
    | Some _, _ -> -1
    | _, Some _ -> 1
    | None, None -> 0
*)

module H = Asak.Lambda_hash
module S = H.FPSet

type mutual_inclusion = {
    a_in_b : float ;
    b_in_a : float ;
  }

let mutual_inclusion common_weight entry_weight =
  fun e1 e2 ->
    let w1 = entry_weight e1 in
    let w2 = entry_weight e2 in
    let c = common_weight e1 e2 in
    { a_in_b = c /. w1 ;
      b_in_a = c /. w2 ;
    }

let avg mi = (mi.a_in_b +. mi.b_in_a) /. 2.
let standard_deviation mi =
  let avg = avg mi in
  sqrt (0.5 *. (((mi.a_in_b -. avg) ** 2.) +. ((mi.b_in_a -. avg) ** 2.)))

type funs =
  { weight : (H.fingerprint -> float) ;
    entry_weight : Db.Entry.t -> float ;
    common_weight : Db.Entry.t -> Db.Entry.t -> float ;
    mutual_inclusion : Db.Entry.t -> Db.Entry.t -> mutual_inclusion ;
  }
let funs db weight =
  let weight = Weight.cached_weight (weight db) in
  let common_weight = Weight.common_weight weight in
  let entry_weight = Weight.entry_weight weight in
  let mutual_inclusion = mutual_inclusion common_weight entry_weight in
  { weight ; common_weight ; entry_weight ; mutual_inclusion }


    