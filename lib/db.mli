

module FPSet : Set.S with type elt = Asak.Lambda_hash.fingerprint
module DigMap : Map.S with type key = Digest.t

exception Invalid_database of string * string

module Entry :
  sig
    type t = {
        name : string;
        loc : Warnings.loc;
        weight: int ;
        hash : Asak.Lambda_hash.hash ;
        code : Lambda.lambda option ;
      }
    val compare : 'a -> 'a -> int
  end

module Entries : Set.S with type elt = Entry.t
module EntryMap : Map.S with type key = Entry.t

type t

val create : Hash.options -> Entries.t -> t
val load_db : Lwt_io.file_name -> t Lwt.t
val entries : t -> Entries.t
val count : t -> Digest.t -> int
val distinct_digests : t -> int
val entries_by_digest : t -> Digest.t -> Entries.t
val global_fields : t -> int

val load_db : string -> t Lwt.t
val store_db : t -> string -> unit Lwt.t

val collect_to_db : Hash.options ->
  ?keep_code:bool ->
  ?modules:bool ->
    ?cmti:bool ->
    ?includes:string list -> ?db:t -> Cmi_cache.SSet.elt list -> t Lwt.t

val build: Options.common_opts ->
  ?add:bool ->
  Hash.options ->
  ?keep_code:bool ->
  ?modules:bool ->
  ?cmti:bool ->
  ?includes:string list ->
  dst:string ->  string list -> unit Lwt.t

val show :
  Options.common_opts -> string -> unit Lwt.t

