
val string_of_threshold : Asak.Lambda_hash.threshold -> string

(** @raise Invalid_argument if the string is invalid. *)
val threshold_of_string : string -> Asak.Lambda_hash.threshold

type options = {
    threshold : Asak.Lambda_hash.threshold ;
    hash_variables : bool ;
    sort_hashes : bool ;
    hash_constants : bool ;
    apply_with_holes : bool ;
  }

val pp_options : Format.formatter -> options -> unit

val load_typedtree : ?includes: string list ->
  string -> (Misc.modname * Typedtree.structure) Lwt.t

val hash_file : options -> ?cmti:bool -> ?includes:string list ->
   string -> ((string * Location.t) * Asak.Lambda_hash.hash * Lambda.lambda) list Lwt.t
