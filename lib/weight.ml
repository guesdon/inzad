
module H = Asak.Lambda_hash
module S = H.FPSet

let rec weight1 = function
| Asak.Lambda_hash.W (w,_,_dig) -> 1. /. float w
| Global_field _ -> 1.
| Holed (_, fp) -> 0.5 *. (weight1 fp)

let weight2 db =
  let nb_digests = float (Db.distinct_digests db) in
  let nb_global_fields = float (Db.global_fields db) in
  let rec f = function
  | Asak.Lambda_hash.W (w,_,dig) ->
      (1. +. 1. /. float w)  *. ((nb_digests -. float (Db.count db dig)) /. nb_digests)
  | Global_field (dig, _, _) ->
      let occ = float (Db.count db dig) in
      3. *. (nb_global_fields -. occ) /. nb_global_fields
  | Holed (_, fp) -> 0.5 *. f fp
  in
  f

let cached_weight weight =
  let cache = ref H.FPMap.empty in
  let f fp =
    match H.FPMap.find_opt fp !cache with
    | Some w -> w
    | None ->
        let w = weight fp in
        cache := H.FPMap.add fp w !cache ;
        w
  in
  f

module CrossEntry =
  struct
    module M = Db.EntryMap
    let empty = M.empty
    let sort_keys e1 e2 =
      match Db.Entry.compare e1 e2 with
      | n when n > 0 -> (e2, e1)
      | _ -> (e1, e2)
    let find_opt e1 e2 map =
      let (e1, e2) = sort_keys e1 e2 in
      match M.find_opt e1 map with
      | None -> None
      | Some m -> M.find_opt e2 m
    let add e1 e2 v map =
      let (e1, e2) = sort_keys e1 e2 in
      let m =
        match M.find_opt e1 map with
        | None -> M.singleton e2 v
        | Some m -> M.add e2 v m
      in
      M.add e1 m map
  end

let set_weight weight s = S.fold
  (fun fp acc -> weight fp +. acc)
    s 0.

let common_weight weight =
  Log.info (fun m -> m "Initializating common-weight cache");
  let set_weight = set_weight weight in
  let cache = ref CrossEntry.empty in
  fun e1 e2 ->
    match CrossEntry.find_opt e1 e2 !cache with
    | Some w -> w
    | None ->
        let common = S.inter e1.Db.Entry.hash e2.hash in
        let w = set_weight common in
        cache := CrossEntry.add e1 e2 w !cache ;
        w

let entry_weight weight =
  Log.info (fun m -> m "Initializating entry-weight cache");
  let set_weight = set_weight weight in
  let cache = ref CrossEntry.empty in
  fun e ->
    match Db.EntryMap.find_opt e !cache with
    | Some w -> w
    | None ->
        let w = set_weight e.Db.Entry.hash in
        cache := Db.EntryMap.add e w !cache ;
        w
