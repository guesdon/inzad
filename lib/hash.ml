let () = Lwt_preemptive.init 0 10 (fun str -> Log.err (fun m -> m "%s" str))

let string_of_threshold = function
| Asak.Lambda_hash.Percent n -> Printf.sprintf "%d%%" n
| Hard n -> string_of_int n

let threshold_of_string str =
  match int_of_string_opt str with
  | Some n ->
      if n > 0
      then Asak.Lambda_hash.Hard n
      else invalid_arg (Printf.sprintf "Invalid hard threshold %S" str)
  | None ->
      let f n =
        if n >= 0 && n <= 100
        then Asak.Lambda_hash.Percent n
        else invalid_arg (Printf.sprintf
           "Invalid threshold %S: must be between 0%% and 100%%" str)
      in
      try Scanf.sscanf str "%d%%" f
      with Scanf.Scan_failure _ ->
          invalid_arg (Printf.sprintf "Invalid threshold %S" str)


type options = {
    threshold : Asak.Lambda_hash.threshold ;
    hash_variables : bool ;
    sort_hashes : bool ;
    hash_constants : bool ;
    apply_with_holes : bool ;
  }

let pp_options fmt o =
  Format.fprintf fmt "threshold: %s, hash_variables: %b, sort_hashes: %b"
    (string_of_threshold o.threshold) o.hash_variables o.sort_hashes

let add_parens_if_needed =
  let special_infix_ops =
    ["asr"; "land"; "lor"; "lsl"; "lsr"; "lxor"; "mod"; "or"; ":="; "!="; "::" ]
  in
  let add_par len str last_dot =
    let p = match last_dot with
    | None -> -1
    | Some p -> p
    in
    let pref = String.sub str 0 (max 0 p) in
    let op = String.sub str (p+1) (len - p - 1) in
    let sp1 = if String.length op > 0 && op.[0] = '*' then " " else "" in
    let sp2 = if String.length op > 0 && op.[String.length op - 1] = '*' then " " else "" in
    Printf.sprintf "%s.(%s%s%s)"
      pref sp1 op sp2
  in
  let rec iter len str last_dot prev_dot i =
    if i >= len then
      (* check for special infix operators *)
      let last_id =
        let p = match last_dot with
          | None -> -1
          | Some p -> p
        in
        String.sub str (p+1) (len - p - 1)
      in
      if List.mem last_id special_infix_ops then
        add_par len str last_dot
      else
        str
    else
      let c = str.[i] in
      match c with
      | 'a'..'z' | 'A'..'Z' | '0'..'9' | '\'' | '_' ->
          iter len str last_dot false (i+1)
      | '.' ->
          if prev_dot then
            add_par len str last_dot
          else
            iter len str (Some i) true (i+1)
      | _ -> add_par len str last_dot
  in
  fun str -> iter (String.length str) str None false 0

let filter_lambdas =
  let pred file env ((name, _), _) =
    match
      let name2 = add_parens_if_needed name in
      Log.debug
        (fun m -> if name <> name2 then
             m "%s: %s -> %s" file name name2);
      match Parse.longident (Lexing.from_string name2) with
      | exception _ ->
          Log.err (fun m -> m "%s: Invalid longident %S" file name2);
          None
      | li -> Some li
    with
    | None -> false
    | Some li ->
        match Env.find_value_by_name li env with
        | exception Not_found -> false
        | _ -> true
  in
  fun file env -> List.filter (pred file env)

(* mutex for ocaml compiler lib access *)
let ocaml_mutex = Lwt_mutex.create ()

(** Replace "__<char>" in modname with ".<uppercase char>". *)
let modname_to_dot =
  let re = Re.(compile (seq [str "__"; group (alt [ rg 'a' 'z' ; rg 'A' 'Z'])])) in
  let f g = "." ^ String.uppercase_ascii (Re.Group.get g 1) in
  fun s -> Re.replace ~all:true re ~f s

let load_path_init xs = Load_path.(init ~auto_include:no_auto_include xs)

let read_cmt cmt_file =
  Log.info (fun m -> m "Loading %s" cmt_file);
  Cmt_format.read_cmt cmt_file

let read_signature_from_cmti cmti_file =
  try%lwt
    let%lwt cmt = Lwt_preemptive.detach read_cmt cmti_file in
    match cmt.Cmt_format.cmt_annots with
    | Interface sign -> Lwt.return_some sign
    | _ ->
        Log.err (fun m -> m "%s: not a signature!" cmti_file);
        Lwt.return_none
  with
  | e ->
      let msg = match e with
        | Sys_error msg -> msg
        | e -> Printexc.to_string e
      in
      Log.warn (fun m -> m "%s" msg) ;
      Lwt.return_none

let load_typedtree_no_mutex ?(includes=[]) cmt_file =
  let%lwt cmt = Lwt_preemptive.detach read_cmt cmt_file in
  let cmt_dir = Filename.dirname cmt_file in
  let%lwt dirs =
    let f acc = function
    | (_, None) -> Lwt.return acc
    | (modname, Some digest) ->
        match%lwt Cmi_cache.find_cmi cmt_dir digest with
        | None ->
            Log.debug (fun m -> m "Did not find %s (%s) in digests"
               (Digest.to_hex digest) modname);
            Lwt.return acc
        | Some f ->
            Lwt.return ((Filename.dirname f)::acc)
    in
    Lwt_list.fold_left_s f [] cmt.Cmt_format.cmt_imports
  in
  match cmt.Cmt_format.cmt_annots with
  | Implementation structure ->
      let loadpath = cmt_dir :: dirs @ cmt.cmt_loadpath in
      let loadpath = includes @ loadpath in
      Log.debug (fun m -> m "load_path_init with loadpath=%s"
         (String.concat ", " loadpath));
      Clflags.recursive_types := Array.mem "-rectypes" cmt.cmt_args ;
      let structure =
        try
          Asak.Parse_structure.init_path ();
          load_path_init loadpath;
          let map =
            { Tast_mapper.default
              with env = fun _ env -> Envaux.env_of_only_summary env } in
          let structure = map.structure map structure in
          structure
        with
          | e ->
            let f =
              match e with
              | Envaux.Error e ->
                  (fun pp () -> Envaux.report_error pp e)
              | Persistent_env.Error e ->
                  (fun pp () -> Persistent_env.report_error pp e)
              | e -> fun pp () -> Format.fprintf pp "%s" (Printexc.to_string e)
            in
            Log.err (fun m -> m "File %s: %a" cmt_file f ());
            structure
      in
      Lwt.return (modname_to_dot cmt.cmt_modname, structure)
  | _ -> failwith (Printf.sprintf "%s: not a structure!" cmt_file)

let hash_file options ?(cmti=false) ?includes cmt_file =
  let%lwt (cmt_file, sign) =
    match Filename.extension cmt_file with
    | ".cmti" ->
        let%lwt sign = read_signature_from_cmti cmt_file in
        Lwt.return ((Filename.remove_extension cmt_file)^".cmt", sign)
    | _ ->
        let%lwt sign =
          if cmti then
            read_signature_from_cmti
              ((Filename.remove_extension cmt_file)^".cmti")
          else
            Lwt.return_none
        in
        Lwt.return (cmt_file, sign)
  in
  Log.info (fun m -> m "Hashing %s" cmt_file);
  let%lwt lambdas =
    Lwt_mutex.with_lock ocaml_mutex
      (fun () ->
         let%lwt (modname, tt) = load_typedtree_no_mutex ?includes cmt_file in
         match Asak.Parse_structure.read_structure_with_loc ~prefix:modname tt with
         | exception e ->
             let f = match e with
               | Envaux.Error e ->
                   (fun pp () -> Envaux.report_error pp e)
               | Env.Error e ->
                   (fun pp () -> Env.report_error pp e)
               | Persistent_env.Error e ->
                   (fun pp () -> Persistent_env.report_error pp e)
               | e -> fun pp () -> Format.fprintf pp "%s" (Printexc.to_string e)
             in
             Log.err (fun m -> m "%s:@[%a@]@ @[%s@]"
                  cmt_file f ()
                  "Returning an empty list of fingerprints.");
             Log.debug (fun m ->
                let s = Printexc.get_backtrace () in m "backtrace: %s" s);
             Lwt.return []
         | lambdas ->
             let lambdas = match sign with
               | None -> lambdas
               | Some sign -> filter_lambdas cmt_file sign.Typedtree.sig_final_env lambdas
             in

             let lambdas = Asak.Lambda_hash.map_snd
               (fun x -> Asak.Lambda_normalization.(normalize_local_variables (inline_all x)))
                 lambdas
             in
             Lwt.return lambdas
      )
  in
  let hash_list =
    Asak.Lambda_hash.(hash_all
     { should_sort = options.sort_hashes;
       hash_var = options.hash_variables;
       hash_const = options.hash_constants ;
       apply_holes = options.apply_with_holes ;
     }
       options.threshold lambdas)
  in
  let list = List.map2 (fun (x,h) (_,code) -> (x, h, code))
    hash_list lambdas
  in
  List.iter (fun ((str,loc), hashes, _code) ->
     Log.debug (fun m -> m "%s [%a]: %a"
        str Log.pp_loc loc Asak.Lambda_hash.pp_top_fingerprint hashes)
  )
    list;
  Lwt.return list

let load_typedtree ?includes cmt_file =
  Lwt_mutex.with_lock ocaml_mutex
    (fun () -> load_typedtree_no_mutex ?includes cmt_file)
