module IMap : Map.S with type key = int

type approx =
| Sharing of Asak.Lambda_hash.threshold
| Includes of float (* threshold 0..1 *)
| Included of float (* threshold 0..1 *)
| Mutual_include of float * float (* threshol 0..1 and standard deviation *)

type approx_result =
| RSharing of Asak.Lambda_hash.threshold * Db.Entries.t IMap.t
| RIncludes of float * (Db.Entry.t * float) list
| RIncluded of float * (Db.Entry.t * float) list
| RMutual_include of float * float * (Db.Entry.t * float * float) list

type find_result =
  { e : Db.Entry.t ;
    same : Db.Entry.t list ;
    approx : approx_result option ;
  }

val add_src_to_dbs :
  Hash.options -> string list -> Db.t -> Db.t Lwt.t

val entries_sharing_fp_with : Db.t -> Db.Entry.t -> Db.Entry.t list

val find_approx : Db.t -> Inclusion.funs -> Db.Entry.t -> approx -> approx_result

val find_for_entry :
  Db.t -> Inclusion.funs -> ?approx:approx -> Db.Entry.t -> find_result Lwt.t

val find :
  Options.common_opts ->
  Hash.options ->
  ?keep_code:bool ->
  ?approx:approx ->
  ?cmti:bool ->
  ?includes:string list ->
  dbs:string list ->  string list -> find_result list Lwt.t

val print_find_result : ?fmt:Format.formatter -> ?limit:int -> find_result -> unit