[@@@landmark "auto"]

module IMap = Map.Make(Int)

type approx =
| Sharing of Asak.Lambda_hash.threshold
| Includes of float
| Included of float
| Mutual_include of float * float (* threshold * standard deviation *)

type approx_result =
| RSharing of Asak.Lambda_hash.threshold * Db.Entries.t IMap.t
| RIncludes of float * (Db.Entry.t * float) list
| RIncluded of float * (Db.Entry.t * float) list
| RMutual_include of float * float * (Db.Entry.t * float * float) list

type find_result =
  { e : Db.Entry.t ;
    same : Db.Entry.t list ;
    approx : approx_result option ;
  }

let find_same db e =
  match Asak.Lambda_hash.top_digest e.Db.Entry.hash with
  | None -> []
  | Some dig ->
      let entries = Db.entries_by_digest db dig in
      Db.Entries.fold
        (fun e2 acc ->
           match Db.Entry.compare e e2 with
           | 0 -> acc
           | _ ->
               match Asak.Lambda_hash.top_digest e2.Db.Entry.hash with
               | None -> acc
               | Some dig2 ->
                   if dig2 = dig then e2 :: acc else acc
        )
        entries []

(* Look for entries which have at least one fingerprint common to e *)
let entries_sharing_fp_with db e =
  Log.debug (fun m -> m "Find.entries_sharing_fp_with %s" e.Db.Entry.name);
  let module H = Asak.Lambda_hash in
  match H.top_fingerprint e.Db.Entry.hash with
  | None -> (* lambda was cut, no hash, nothing to do *)
      []
  | Some _ ->
      let entries =
        H.FPSet.fold
          (fun fp acc ->
             let dig = H.fingerprint_digest fp in
             let common = Db.entries_by_digest db dig in
             let card = Db.Entries.cardinal common in
             if card <= 0 || card >= 1000 then
               (* we use a upper bound because a fingerprint common to
                  a lot of functions may not be very relevant and
                  the relevant functions will be found with
                  more relevant fingerprints in common with e *)
               acc
             else
               (
                (*prerr_endline (Printf.sprintf "Union %d %d"
                  (Db.Entries.cardinal acc) card);*)
                Db.(Entries.union acc common)
               )
          )
          e.Db.Entry.hash Db.Entries.empty
      in
      let entries = Db.Entries.remove e entries in
      Db.Entries.elements entries
(*
let find_dist db sim th e =
  Log.debug (fun m -> m "find_dist for %s" e.Db.Entry.name);
  let module H = Asak.Lambda_hash in
  let sim = sim db in
  match H.top_fingerprint e.Db.Entry.hash with
  | None -> (* lambda was cut, no hash, nothing to do *)
      []
  | Some _ ->
      (* look for entries which have at least one fingerprint common to e *)
      let entries =
        H.FPSet.fold
          (fun fp acc ->
             let dig = H.fingerprint_digest fp in
             let common = Db.entries_by_digest db dig in
             let card = Db.Entries.cardinal common in
             if card <= 0 || card >= 1000 then
               (* we use a upper bound because a fingerprint common to
                  a lot of functions may not be very relevant and
                  the relevant functions will be found with
                  more relevant fingerprints in common with e *)
               acc
             else
               (
                (*prerr_endline (Printf.sprintf "Union %d %d"
                  (Db.Entries.cardinal acc) card);*)
                Db.(Entries.union acc common)
               )
          )
          e.Db.Entry.hash Db.Entries.empty
      in
      let entries = Db.Entries.remove e entries in
      if Db.Entries.cardinal entries <= 0 then
        []
      else
        (
         Log.debug (fun m -> m "%s %a has at least one digest in common with %d entries in db"
            e.name Log.pp_loc e.loc (Db.Entries.cardinal entries));
         let l = Db.Entries.fold
           (fun e2 acc ->
              let (score, _, _) = sim.Dist.compute e e2 in
              if score >= th then (e2, score) :: acc else acc)
             entries []
         in
         let compare (e1,d1) (e2,d2) =
           match Stdlib.compare d1 d2 with
           | 0 -> Db.Entry.compare e1 e2
           | n -> n
         in
         List.sort compare l
        )
*)

let mutual_inc db funs e =
  let l = Db.Entries.fold
    (fun e2 acc ->
       if Db.Entry.compare e e2 = 0 then
         acc
       else
         let mi = funs.Inclusion.mutual_inclusion e e2 in
         (e2, Inclusion.avg mi, Inclusion.standard_deviation mi) :: acc
    )
      (Db.entries db) []
  in
  List.sort (fun (_,avg1,_) (_,avg2,_) -> Float.compare avg2 avg1) l

let find_approx db funs e = function
| Sharing th -> RSharing (th, IMap.empty)
| Mutual_include (th, sd) ->
    let l = mutual_inc db funs e in
    RMutual_include (th, sd, l)
| Includes _th ->
    (*Rincludes (th, find_includes db proximity th e)*)
    failwith "find_approx Includes not implemented"
| Included _th ->
    (*Rincluded (th, find_included db proximity th e)*)
    failwith "find_approx Included not implemented"

let find_for_entry db funs ?approx e =
  Log.debug (fun m -> m "find_for_entry %a"
     Fmt.(styled `Bold string) e.Db.Entry.name) ;
  let%lwt () = Lwt.pause () in
  let same = find_same db e in
  let%lwt () = Lwt.pause () in
  let approx = Option.map (find_approx db funs e) approx in
  let r = { e ; approx ; same } in
  Lwt.return r

let print_find_result ?(fmt=Format.std_formatter) ?(limit=max_int) r =
  match r.approx with
  | None ->
      (* print same functions *)
      begin
        match r.same with
        | [] -> ()
        | l ->
            Fmt.pf fmt "%a" Fmt.(styled `Bold string) r.e.name ;
            let len = List.length l in
            Fmt.pf fmt " [%a] has same hash as %d function%s:\n"
              Log.pp_loc r.e.loc len (if len > 1 then "s" else "") ;
            List.iter (fun e ->
               Fmt.pf fmt "=> %a [%a]\n"
                 Fmt.(styled `Italic string) e.Db.Entry.name
                 Log.pp_loc e.loc)
              l;
            Format.pp_print_newline fmt ()
      end
  | Some (RIncludes (_th, _l))
  | Some (RIncluded (_th, _l)) ->
     ()
(*     (
       match l with
       | [] -> ()
       | _ ->
            let len = List.length l in
            Log.app (fun m -> m "%a [%a] has similarity >= %.2f to %d function%s:"
              Fmt.(styled `Bold string) r.e.name Log.pp_loc r.e.loc th len
              (if len > 1 then "s" else "")
            ) ;
            List.iter (fun (e,d) ->
               Log.app (fun m -> m "=> %a [%a] (%.2f)"
                 Fmt.(styled `Italic string) e.Db.Entry.name
                 Log.pp_loc e.loc d))
              l;
            Format.pp_print_newline fmt ()
     )
*)
  | Some (RMutual_include (th,sd,l)) ->
      let l = List.filter
        (fun (e,score,score_sd) ->
           let w = Asak.Lambda_hash.top_weight e.Db.Entry.hash in
           w >= 7 && score >= th && score_sd <= sd) l
      in
      (match l with
       | [] -> ()
       | _ ->
           Log.app (fun m -> m  "%a [%a] is similar by mutual inclusion to at least %f (sd=%f) of the following functions:"
              Fmt.(styled `Bold string) r.e.name Log.pp_loc r.e.loc th sd)  ;
           let _ = List.fold_right
             (fun (e,score,score_sd) nb ->
                if  nb < limit then
                  begin
                    Log.app (fun m -> m "  => %a [%a] (%f, sd=%f)"
                       Fmt.(styled `Italic string) e.Db.Entry.name
                         Log.pp_loc e.loc score score_sd);
                    nb + 1
                  end
                else
                  nb
             )
               l 0
           in
           ()
      )
  | Some (RSharing ((Asak.Lambda_hash.Percent n) as th, shared))
  | Some (RSharing ((Asak.Lambda_hash.Hard n) as th, shared)) ->
      let n_shared =
        match IMap.max_binding_opt shared with
        | None -> -1
        | Some x -> fst x
      in
      if n <= n_shared then
        (
         let th =
           match th with
           | Asak.Lambda_hash.Percent n -> Printf.sprintf "%d%%" n
           | Asak.Lambda_hash.Hard n->
               Printf.sprintf "%d hash%s" n
                 (if n > 1 then "es" else "")
         in
         Log.app (fun m -> m  "%a [%a] includes at least %s of the following functions:"
            Fmt.(styled `Bold string) r.e.name Log.pp_loc r.e.loc th)  ;
         let _ = IMap.fold
           (fun k set nb ->
              if k >= n && nb < limit then
                begin
                  Db.Entries.iter
                    (fun e ->
                       Log.app (fun m -> m "  => %a [%a] (%d)"
                         Fmt.(styled `Italic string) e.name
                         Log.pp_loc e.loc k)
                    )
                    set;
                 nb + 1
               end
             else
               nb
           )
           shared 0
         in
         (*         Format.pp_print_newline fmt ()*)
         ()
        )

let add_src_to_dbs options dbs db_src =
  (* we just load and merge databases; should we check that all databases
     have the same options ? *)
  let%lwt entries =
    Lwt_list.fold_left_s
      (fun acc file ->
         let%lwt d = Db.load_db file in
         Log.debug (fun m -> m "merging entries sets");
         Lwt.return (Db.(Entries.union (entries d) acc))
         (*Lwt_preemptive.detach
            (fun () -> Db.(Entries.union (entries d) acc))
            ()*)
      )
      (Db.entries db_src) dbs
  in
  Lwt.return (Db.create options entries)

let find _copts options ?keep_code ?approx ?cmti ?includes ~dbs src =
  let%lwt db_src = Db.collect_to_db options ?keep_code ?cmti ?includes src in
  Log.debug (fun m -> m "db_src has %d entries" (Db.(Entries.cardinal (entries db_src))));
  let%lwt db = add_src_to_dbs options dbs db_src in
  Log.debug (fun m -> m "looking for entries");
  let%lwt () = Lwt.pause () in
  let funs = Inclusion.funs db Weight.weight2 in
  let%lwt results = Lwt_list.map_s (find_for_entry db funs ?approx)
    (Db.Entries.elements (Db.entries db_src))
  in
  Log.debug (fun m -> m "results computed");
  Lwt.return results
