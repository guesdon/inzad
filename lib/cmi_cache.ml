
module SMap = Map.Make(String)
module SSet = Set.Make(String)

type t =
    {
      mutable digests : string SMap.t ; (* digest -> filename *)
      mutable dir_reading : unit Lwt.t SMap.t ; (* directory -> lwt thread to wait on *)
      mutable dir_done : SSet.t ; (* directories already read *)
    }

module Cache :
  sig
    val app : (t -> 'a Lwt.t) -> 'a Lwt.t
  end
  =
    struct
      let mutex = Lwt_mutex.create ()
      let cache = {
          digests = SMap.empty ;
          dir_reading = SMap.empty ;
          dir_done = SSet.empty ;
        }

      let app f = Lwt_mutex.with_lock mutex (fun () -> f cache)
    end

let read_cmi_crc filename = 
  try 
    let cmi = Cmi_format.read_cmi filename in
    (* the cmi's crc is supposed to be the one associated to the
       first element of the crcs list *)
    match cmi.cmi_crcs with
    | (_, crc_opt) :: _ -> crc_opt
    | _ -> None
  with _ -> None

let add_file filename =
  match Filename.extension filename with
  | ".cmi" ->
      (match%lwt Lwt_preemptive.detach read_cmi_crc filename with
       | None -> Lwt.return_unit
       | Some digest ->
           Cache.app (fun cache ->
              Log.debug (fun m -> m "Add to cmi cache: %s -> %s"
                 (Digest.to_hex digest) filename);
              cache.digests <- SMap.add digest filename cache.digests;
              Lwt.return_unit)
      )
  | _ -> Lwt.return_unit

let rec read_files_and_dirs =
  let f path entry =
    if entry = Filename.current_dir_name
      || entry = Filename.parent_dir_name
    then
      Lwt.return_unit
    else
      let filename = Filename.concat path entry in
      match%lwt Lwt_unix.stat filename with
      | exception Unix.Unix_error (e,s1,s2) ->
          Log.err (fun m -> m "%s: %s %s" s1 (Unix.error_message e) s2);
          Lwt.return_unit
      | { st_kind = Unix.S_DIR } ->
          add_dir filename
      | { st_kind = Unix.S_REG } ->
          add_file filename
      | _ ->
          Log.warn (fun m -> m "Ignoring %s" filename);
          Lwt.return_unit
  in
  fun path entries -> Lwt_list.iter_p (f path) entries

and read_dir wakener dir =
  Log.debug (fun m -> m "read_dir %s" dir);
  let entries = Lwt_unix.files_of_directory dir in
  let%lwt entries = Lwt_stream.to_list entries in
  let%lwt () = read_files_and_dirs dir entries in
  Cache.app (fun cache ->
       cache.dir_reading <- SMap.remove dir cache.dir_reading ;
       cache.dir_done <- SSet.add dir cache.dir_done ;
       Lwt.wakeup wakener ();
       Lwt.return_unit)

and add_dir dir =
  match%lwt
    Cache.app (fun cache ->
       if SSet.mem dir cache.dir_done then
         Lwt.return (`Done Lwt.return_unit)
       else
         match SMap.find_opt dir cache.dir_reading with
         | None ->
             let waiter, wakener = Lwt.wait () in
             cache.dir_reading <- SMap.add dir waiter cache.dir_reading ;
             Lwt.return (`Todo wakener)
         | Some t -> Lwt.return (`Done t)
    )
  with
  | `Done t -> t
  | `Todo wakener -> read_dir wakener dir

let find_cmi root_dir digest =
  let%lwt () = add_dir root_dir in
  Cache.app (fun cache -> Lwt.return (SMap.find_opt digest cache.digests))
