let src = Logs.Src.create ~doc:"log of the Inzad library" "inzad"

let err f = Logs.err ~src f
let warn f = Logs.warn ~src f
let info f = Logs.info ~src f
let debug f = Logs.debug ~src f
let app f = Logs.app ~src f

let pp_loc = Fmt.(styled (`Faint) Location.print_loc)
