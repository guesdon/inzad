
let is_cmt file = Filename.extension file = ".cmt"
let is_cmti file = Filename.extension file = ".cmti"

(** We keep the same fingerprint type as Asak. *)
type fingerprint = Asak.Lambda_hash.fingerprint

module FP_order = struct
    type t = fingerprint
    let compare = Stdlib.compare
  end
module FPSet = Asak.Lambda_hash.FPSet

module DigMap = Map.Make(Digest)
module DigSet = Set.Make(Digest)

module Entry =
  struct
    type t =
      { name: string ;
        loc: Location.t ;
        weight: int ;
        hash: Asak.Lambda_hash.hash ;
        code: Lambda.lambda option ;
      }
    let compare = Stdlib.compare
  end
module Entries = Set.Make(Entry)
module EntryMap = Map.Make(Entry)

type t = {
  options : Hash.options ;
  entries : Entries.t ;
  counts : int DigMap.t ;
  global_fields : int ; (* number of Global_field fingerprints *)
  reverse : Entries.t DigMap.t ;
}

exception Invalid_database of string * string (** Filename * message *)
let () = Printexc.register_printer
  (function Invalid_database (file, msg) ->
       Some (Printf.sprintf "%s: %s" file msg)
   | _ -> None)

let read_file file =
  let%lwt ic = Lwt_io.open_file ~mode:Input file in
  let b = Buffer.create 1_000_000 in
  let rec iter n =
    match%lwt Lwt_io.read ~count:10_000 ic with
    | "" -> Lwt.return_unit
    | s ->
        let n = n + String.length s in
        Log.debug (fun m -> m "read %d bytes" n);
        Buffer.add_string b s; iter n
  in
  let%lwt () = iter 0 in
  let%lwt () = Lwt_io.close ic in
  Lwt.return (Buffer.contents b)

let load_db file : t Lwt.t =
  Log.debug (fun m -> m "load db %S" file);
  try%lwt
    let%lwt s = Lwt_io.(with_file ~mode:Input file read) in
    Log.debug (fun m -> m "Marshal db from string");
    let v = Marshal.from_string s 0 in
    Lwt.return v
  with
  | Failure msg
  | Sys_error msg -> raise (Invalid_database (file, msg))
  | Unix.Unix_error (e,_,_) ->
      raise (Invalid_database (file, Unix.error_message e))

let store_db db file =
  Log.info (fun m -> m "Storing db to %S" file);
  let s = Marshal.to_string db [] in
  Lwt_io.(with_file ~mode:Output file
   (fun oc -> write oc s))

let store_db_dummy db file =
  Log.info (fun m -> m "Storing db to %S" file);
  let s = Marshal.to_string db [] in
  Lwt_io.(with_file ~mode:Output file
   (fun oc -> write_line oc s))

let option_map f = function None -> None | Some x -> Some (f x)

let asak_hash_to_set (fprint,fprints) = FPSet.of_list (fprint::fprints)

let digest_count_incr hash map =
  let dig =
    match hash with
    | Asak.Lambda_hash.W (_,_,dig)
    | Global_field (dig, _, _) -> dig
    | Holed (dig,_) -> dig
  in
  DigMap.update dig (function None -> Some 1 | Some n -> Some (n + 1)) map

let digests_count_incr set map =
  FPSet.fold digest_count_incr set map

let entries_digests_count_incr =
  Entries.fold
    (fun e map -> digests_count_incr e.hash map)

let reverse_digest_map =
  let add_dig e fp acc =
    let dig =
      match fp with
      | Asak.Lambda_hash.W (_,_,dig)
      | Global_field (dig, _, _) -> dig
      | Holed (dig, _) -> dig
    in
    let set =
      match DigMap.find_opt dig acc with
      | None -> Entries.empty
      | Some s -> s
    in
    let set = Entries.add e set in
    DigMap.add dig set acc
  in
  let f = Entries.fold
    (fun e acc ->
      FPSet.fold (add_dig e) e.Entry.hash acc
    )
  in
  fun entries -> f entries DigMap.empty

let fill_global_fields db =
  let f_hash hash gf =
    match hash with
    | Asak.Lambda_hash.Global_field _ -> gf + 1
    | _ -> gf
  in
  let f_entry e acc =
    Asak.Lambda_hash.FPSet.fold f_hash e.Entry.hash acc
  in
  let global_fields = Entries.fold f_entry db.entries 0 in
  { db with global_fields }

let create options entries =
  let db = { options ; entries ;
      counts = entries_digests_count_incr entries DigMap.empty ;
      global_fields = 0 ;
      reverse = reverse_digest_map entries ;
    }
  in
  fill_global_fields db

let entries t = t.entries
let count t dig =
  match DigMap.find_opt dig t.counts with
  | None -> 0
  | Some n -> n
let distinct_digests t = DigMap.cardinal t.counts
let entries_by_digest db dig =
  match DigMap.find_opt dig db.reverse with
  | None -> Entries.empty
  | Some set -> set
let global_fields db = db.global_fields

let collect_file acc options ?(keep_code=false) ?(modules=false)
  ?cmti ?includes file =
  let%lwt hashes = Hash.hash_file options ?cmti ?includes file in
  let entries = List.map
    (fun ((name, loc), hash, code) ->
       let code = if keep_code then Some code else None in
       let weight = Asak.Lambda_hash.top_weight hash in
       { Entry.name ; loc ; weight ; hash ; code })
      hashes
  in
  if modules then
    let dbfile = Printf.sprintf "%s.%s"
      (Filename.remove_extension file) Options.inzad_file_ext
    in
    let entries = Entries.of_list entries in
    let db = create options entries in
    let%lwt () = store_db db dbfile in
    Lwt.return (Entries.union acc entries)
  else
    Lwt.return
      (List.fold_left (fun acc elt -> Entries.add elt acc) acc entries)

let rec collect_direntry acc options ?keep_code ?modules ?cmti ?includes path entry =
  if entry = Filename.current_dir_name
  || entry = Filename.parent_dir_name
  then Lwt.return acc
  else
    let filename = Filename.concat path entry in
    collect_file_or_dir acc options ?keep_code ?modules ?cmti ?includes filename

and collect_file_or_dir acc options ?(explicit=false)
  ?keep_code ?modules ?cmti ?includes filename =
  match%lwt Lwt_unix.stat filename with
  | exception Unix.Unix_error (e,s1,s2) ->
      Log.err (fun m -> m "%s: %s %s" s1 (Unix.error_message e) s2);
      Lwt.return acc
  | { st_kind = Unix.S_DIR } ->
      collect_dir acc options ?keep_code ?modules ?cmti ?includes filename
  | { st_kind = Unix.S_REG } when is_cmt filename ->
      collect_file acc options ?keep_code ?modules ?cmti ?includes filename
  | { st_kind = Unix.S_REG } when is_cmti filename && explicit ->
      collect_file acc options ?keep_code ?modules ?cmti ?includes filename
  | { st_kind = Unix.S_REG } ->
      Lwt.return acc
  | _ ->
      Log.warn (fun m -> m "Ignoring %s" filename);
      Lwt.return acc

and collect_dir acc options ?keep_code ?modules ?cmti ?includes dir =
  let entries = Lwt_unix.files_of_directory dir in
  let sets = Lwt_stream.map
          (collect_direntry Entries.empty options
           ?keep_code ?modules ?cmti ?includes dir) entries
  in
  let%lwt sets = Lwt_stream.to_list sets in
  let%lwt acc = Lwt_list.fold_left_s
    (fun acc lwt -> let%lwt set = lwt in Lwt.return (Entries.union acc set))
    acc sets
  in
  Lwt.return acc

let empty_db options =
  { options ;
    entries = Entries.empty ;
    counts = DigMap.empty ;
    global_fields = 0 ;
    reverse = DigMap.empty ;
  }

let check_options file db options =
  let open Hash in
  let diffs =
    if db.options.threshold <> options.threshold then
      [Printf.sprintf "Required threshold is %s but threshold used in %s is %s."
        (Hash.string_of_threshold options.threshold)
        file
        (Hash.string_of_threshold db.options.threshold)]
    else
      []
  in
  let diffs =
    if db.options.hash_variables <> options.hash_variables then
      (Printf.sprintf "Hashing of variables differ in options (%b) and in %s (%b)."
         options.hash_variables file db.options.hash_variables) :: diffs
    else
      diffs
  in
  let diffs =
    if db.options.sort_hashes <> options.sort_hashes then
      (Printf.sprintf "Sorting of hashes differ in options (%b) and in %s (%b)."
         options.hash_variables file db.options.hash_variables) :: diffs
    else
      diffs
  in
  match diffs with
  | [] -> ()
  | _ -> raise (Types.(Error (Options_differ diffs)))

let collect_to_db options ?keep_code ?modules ?cmti ?includes ?(db=empty_db options) src =
  let dirs = List.filter (fun src -> try Sys.is_directory src with _ -> false) src in
  let%lwt () = Lwt_list.iter_p Cmi_cache.add_dir dirs in
  let%lwt sets = Lwt_list.map_p
    (collect_file_or_dir Entries.empty options
     ~explicit:true ?keep_code ?modules ?cmti ?includes) src
  in
  let entries = List.fold_left Entries.union db.entries sets in
  Lwt.return (create options entries)

let build _copts ?(add=false) options ?keep_code ?modules ?cmti ?includes ~dst src =
  let%lwt db =
    if add then
      try%lwt load_db dst
      with Invalid_database (file, msg) ->
        Log.warn (fun m -> m "%s: %s" file msg);
        Lwt.return (empty_db options)
    else
      Lwt.return (empty_db options)
  in
  check_options dst db options ;
  let%lwt db = collect_to_db options ?keep_code ?modules ?cmti ?includes ~db src in
  store_db db dst

let show _copts file =
  let fmt = Format.std_formatter in
  let%lwt db = load_db file in
  Format.fprintf fmt "Database %s:\n %d elements\n %d distinct fingerprints\n"
    file (Entries.cardinal db.entries)
    (distinct_digests db);
    Format.fprintf fmt "Options: %a\nElements:\n" Hash.pp_options db.options;
  Entries.iter
    (fun { Entry.name ; loc ; hash } ->
       Format.fprintf fmt "%s [%a]: %d fingerprints\n"
         name Log.pp_loc loc
         (1 + FPSet.cardinal hash)
    ) db.entries;
  Lwt.return_unit


(*
let find_for_entry db ?sharing e =
  let shared =
    match sharing with
    | Some sharing ->
        let in_pct = match sharing with
          | Asak.Lambda_hash.Hard _ -> None
          | Asak.Lambda_hash.Percent _ ->
              let total_weights = Entries.fold
                (fun e acc ->
                   let ((root_w,_), fp_list) = e.Entry.hash in
                   let w = List.fold_left
                     (fun acc (w,_) -> acc +. float w)
                       (float root_w) fp_list
                   in
                   EntryMap.add e w acc
                )
                  db.entries EntryMap.empty
              in
              Some total_weights
        in
        let fp_list = let (fp, fp_list) = e.Entry.hash in fp :: fp_list in
        let add_e weight e2 acc =
          match Entry.compare e e2 with
          | 0 -> acc
          | _ ->
              let n =
                match EntryMap.find_opt e2 acc with
                | None -> 0.
                | Some n -> n
              in
              let n =
                match in_pct with
                | Some weights ->
                    let total_w = EntryMap.find e2 weights in
                    min 100. (n +. 100. *. (float weight /. total_w))
                | None ->
                    n +. 1.
              in
              EntryMap.add e2 n acc
        in
        let emap = List.fold_left
          (fun acc (weight,dig) ->
             match DigMap.find_opt dig db.reverse with
             | None -> acc
             | Some set -> Entries.fold (add_e weight) set acc
          )
            EntryMap.empty fp_list
        in
        let shared = EntryMap.fold
          (fun e n acc ->
             let n = truncate n in
             let set = match IMap.find_opt n acc with
               | None -> Entries.empty
               | Some s -> s
             in
             let set = Entries.add e set in
             IMap.add n set acc
          )
            emap IMap.empty
        in
        shared
    | None -> IMap.empty
  in
  let same =
    let ((_,dig),_) = e.Entry.hash in
    match DigMap.find_opt dig db.reverse with
    | None -> []
    | Some set ->
        Entries.fold
          (fun e2 acc ->
            match Entry.compare e e2 with
            | 0 -> acc
            | _ ->
                 let ((_,dig2),_) = e2.Entry.hash in
                 if dig2 = dig then e2 :: acc else acc
          )
          set []
  in
  { e ; shared ; same }

let print_find_result fmt ?sharing r =
  match sharing with
  | None
  | Some (Asak.Lambda_hash.Percent 0)
  | Some (Asak.Lambda_hash.Hard 0) ->
      begin
        match r.same with
        | [] -> ()
        | l ->
            Fmt.pf fmt "%a" Fmt.(styled `Bold string) r.e.name ;
            let len = List.length l in
            Fmt.pf fmt " [%a] has same hash as %d function%s:\n"
              Log.pp_loc r.e.loc len (if len > 1 then "s" else "") ;
            List.iter (fun e ->
               Fmt.pf fmt "=> %a [%a]\n"
                 Fmt.(styled `Italic string) e.Entry.name
                 Log.pp_loc e.loc)
              l;
            Format.pp_print_newline fmt ()
      end
  | Some (Asak.Lambda_hash.Percent n)
  | Some (Asak.Lambda_hash.Hard n)
        when n <= fst (IMap.max_binding r.shared) ->
      let th =
        match sharing with
          Some Asak.Lambda_hash.Percent n -> Printf.sprintf "%d%%" n
        | Some Asak.Lambda_hash.Hard n->
            Printf.sprintf "%d hash%s" n
              (if n > 1 then "es" else "")
        | _ -> assert false
      in
      Fmt.pf fmt "%a" Fmt.(styled `Bold string) r.e.name ;
      Fmt.pf fmt
        " [%a] includes at least %s of the following functions:\n"
        Log.pp_loc r.e.loc th  ;
      IMap.iter
        (fun k set ->
           if k >= n then
             begin
               Entries.iter
                 (fun e ->
                   Fmt.pf fmt "  => %a [%a] (%d)\n"
                     Fmt.(styled `Italic string) e.name
                      Log.pp_loc e.loc k
                 )
                 set
             end
        )
        r.shared;
      Format.pp_print_newline fmt ()
  | _ -> ()

let find _copts ?(fmt=Format.std_formatter) options ?sharing ?cmti ?includes ~dbs src =
  let%lwt db_src = collect_to_db options ?cmti ?includes src in
  Log.debug (fun m -> m "db_src has %d entries" (DigMap.cardinal db_src.counts));
  (* we just load and merge databases; should we check that all databases
     have the same options ? *)
  let%lwt db =
    let%lwt entries =
      Lwt_list.fold_left_s
        (fun acc file ->
           let%lwt d = load_db file in
           Lwt.return (Entries.union d.entries acc))
        db_src.entries dbs
    in
    Lwt.return (create options entries)
  in
  let results = List.map (find_for_entry db ?sharing)
    (Entries.elements db_src.entries)
  in
  List.iter (print_find_result fmt ?sharing) results;
  Lwt.return_unit
*)




