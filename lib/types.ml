
type error =
| Options_differ of string list

exception Error of error

let string_of_error = function
| Options_differ diffs ->
    Printf.sprintf "Options must be the same as the ones used in database:\n%s"
      (String.concat "\n" diffs)

let () = Printexc.register_printer
  (function Error e -> Some (string_of_error e) | _ -> None)