
type common_opts = {
  log_level: Logs.level option;
  log_style: Fmt.style_renderer option;
  }

let common_opts log_level log_style =
  { log_level ; log_style }

let inzad_file_ext = "inzad"

open Cmdliner

let copts_t =
  let docs = Manpage.s_common_options in
  let log_style = Fmt_cli.style_renderer ~docs () in
  let log_level = Logs_cli.level ~docs () in
  Term.(const common_opts $ log_level $ log_style)

let includes =
  let doc = "Add a directory to the load path." in
  Arg.(value & opt_all dir [] & info ["I"] ~docv:"INCLUDEDIR" ~doc)

let hash_options threshold dont_hash_variables
  sort_hashes hash_constants apply_with_holes =
    {
      Hash.threshold ;
      hash_variables = not dont_hash_variables;
      sort_hashes ; hash_constants ; apply_with_holes
    }

let hash_opts_t =
  let docs = "Hash options" in
  let threshold =
    let c =
      let docv = "THRESHOLD" in
      let parser s =
        try Ok (Hash.threshold_of_string s)
        with e -> Error (`Msg (Printexc.to_string e))
      in
      let printer f t =
        Format.pp_print_string f (Hash.string_of_threshold t)
      in
      Arg.conv ~docv (parser, printer)
    in
    let doc = "Threshold used to cut lambdas; default is 0 (keep all)" in
    Arg.(value & opt c (Asak.Lambda_hash.Hard 0) & info ["t";"threshold"] ~doc ~docs)
  in
  let dont_hash_variables =
    let doc = "do not hash variables" in
    Arg.(value & flag & info ["dont-hash-variables"] ~doc ~docs)
  in
  let sort_hashes =
    let doc = "sort hashes of each lambda" in
    Arg.(value & flag & info ["sort-hashes"] ~doc ~docs)
  in
  let hash_constants =
    let doc = "hash constants in lambda" in
    Arg.(value & flag & info ["hash-constants"] ~doc ~docs)
  in
  let apply_with_holes =
    let doc = "hash application arguments as holes in lambda" in
    Arg.(value & flag & info ["apply-with-holes"] ~doc ~docs)
  in
  Term.(const hash_options
   $ threshold
     $ dont_hash_variables
     $ sort_hashes
     $ hash_constants
     $ apply_with_holes)

let modules =
  let doc = Printf.sprintf
    "create a .%s file for each .mt file too"
      inzad_file_ext
  in
  Arg.(value & flag & info ["m"; "modules"] ~doc)

let cmti =
  let doc =
    "filter .cmt elements using .cmti, if present"
  in
  Arg.(value & flag & info ["cmti"] ~doc)

let keep_code =
  let doc = "keep code" in
  Arg.(value & flag & info ["keep-code"] ~doc)

let dbs =
  let doc = "load database to find similar functions" in
  Arg.(value & opt_all file [] & info ["d"] ~docv:"DBFILE" ~doc)

let lwt_reporter copts =
  (* beware to set style before defining the lwt_reporter, or style
     will not be taken into account *)
  Fmt_tty.setup_std_outputs ?style_renderer:copts.log_style ();
  let buf_fmt ~like =
    let b = Buffer.create 512 in
    Fmt.with_buffer ~like b,
    fun () -> let m = Buffer.contents b in Buffer.reset b; m
  in
  let app, app_flush = buf_fmt ~like:Fmt.stdout in
  let dst, dst_flush = buf_fmt ~like:Fmt.stderr in
  let reporter = Logs_fmt.reporter ~app ~dst () in
  let report src level ~over k msgf =
    let k () =
      let write () = match level with
        | Logs.App -> Lwt_io.write Lwt_io.stdout (app_flush ())
        | _ -> Lwt_io.write Lwt_io.stderr (dst_flush ())
      in
      let unblock () = over (); Lwt.return_unit in
      Lwt.finalize write unblock |> Lwt.ignore_result;
      k ()
    in
    reporter.Logs.report src level ~over:(fun () -> ()) k msgf;
  in
  { Logs.report = report }

let install_log_reporter copts =
  Logs.set_level copts.log_level;
  Logs.set_reporter (lwt_reporter copts)
