
let clusterize (db : Db.t) =
  let _hashes = Db.(Entries.fold
     (fun e acc -> (e, e.Entry.hash) :: acc)
       (entries db)
       [])
  in
  []
  (*
  Asak.Clustering.cluster hashes
  *)

let pp_cluster =
  let open Asak.Wtree in
  let to_string e = e.Db.Entry.name in
  let p fmt x = Format.fprintf fmt x in
  let rec iter fmt i = function
  | Leaf x ->
      begin
        p fmt "%s" (String.make i ' ');
        List.iter (fun x -> p fmt "%s " (to_string x)) x;
        Format.pp_print_newline fmt ();
      end
  | Node (w,x,y) ->
      begin
        p fmt "%s" (String.make i ' ');
        p fmt "Node %d:" w;
        Format.pp_print_newline fmt ();
        iter fmt (i+1) x;
        iter fmt (i+1) y
      end
  in
  let pclass fmt i x =
    p fmt "Class %d:" i;
    Format.pp_print_newline fmt ();
    iter fmt 1 x
  in
  fun fmt cluster ->
    List.iteri (pclass fmt) cluster

let pp_dot =
  let open Asak.Wtree in
  let to_string e = e.Db.Entry.name in
  let p fmt x = Format.fprintf fmt x in
  let idcpt = ref 0 in
  let genid () = incr idcpt; Printf.sprintf "l%d" !idcpt in
  let rec iter fmt i = function
  | Leaf x ->
      List.map
        (fun e ->
           let id = genid () in
           p fmt "{ rank=sink; %s [label=\"%s\",shape=box]};\n" id (to_string e);
           id
        )
        x
  | Node (w,x,y) ->
      let id = genid () in
      p fmt "%s [label=\"%d\",shape=\"ellipse\"];\n" id w;
      let f id2 =  p fmt "%s -> %s;\n" id id2 in
      let ids1 = iter fmt (i+1) x in
      let ids2 = iter fmt (i+1) y in
      List.iter f ids1;
      List.iter f ids2;
      [id]
  in
  let pclass fmt i x =
    p fmt "subgraph cluster%d {" i;
    Format.pp_print_newline fmt ();
    ignore(iter fmt 1 x);
    p fmt "}\n"
  in
  fun fmt cluster ->
    idcpt := 0;
    p fmt "digraph g {\n";
    p fmt "rankdir=LR;\n";
    List.iteri (pclass fmt) cluster;
    p fmt "}\n"

