#!/bin/bash
OPTIONS="--hash-constants --apply-with-holes"
PREF=`opam var prefix`
DB=all.inzad
if [ -f ${DB} ]; \
  then echo ${DB} exists; \
  else \
   for i in a b c d e f g h i j k l m n o p q r s t u v w x y z; do \
    ./inzad db build ${OPTIONS} --keep-code --verbosity info -a ${DB} ${PREF}/lib/${i}*; \
done; \
fi

./inzad db build inzad.inzad _build/default

./inzad find --verbosity debug -d ${DB} \
  -I _build/default/lib/.inzad.objs/byte \
  -I _build/default/asak/.asak.objs/byte \
  _build/default/lib/ --dist 0.5
