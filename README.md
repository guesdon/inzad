# Inzad

Inzad was an experiment to detect OCaml code similarities using
signatures based on [Asak](https://github.com/nobrakal/asak).

This software is not developed nor maintained but kept here of the records.

### Compilation

```
$ make
```

### Building the database

```
$ inzad db build [OPTION] DB [ARGS]
```
where ARGS is a list of elements where an element can be:

- a directory: all `.cmt` files in this hierarchy will be added to the database.
- a `.cmt` file: this file will be added to the database.
- a `.cmti` file: same as a `.cmt` file, but only the elements the
  corresponding `.cmt` file which appear in the public interface will be
  added to the database.

Moreover, for each directory, all the `.cmi` files in this hierarchy are read
and their digest put in a cache, so that when a `.cmt` file requires a `.cmi` file,
its digest is looked up and, if found, the directory of the `.cmi` file is added
to the `loadpath` of the `.cmt` so that loading it and rebuilding the lambda
representation works.

Whether or not a directory is passed on the command line, loading a `.cmt` file
triggers the lookup of the required `.cmi` files, reading the directory
hierarchy starting from the file directory.

If this is not enough to find the required `.cmi` files, the `-I` option can
be used to add directories to the load path when reading a `.cmt` file.

The `-a` or `--add` option can be used to add elements to a database, allowing incremental
build of the database. (if the database cannot be loaded, a warning is printed)

The `-m` or `--modules` option can be used to create a `.inzad` file for each `.cmt` file.
Such a file is a database with only the elements from the corresponding `.cmt` file.

The `--cmti` option specifies that, if present, the `.cmti` file associated
to a `.cmt` file must be used to filter elements from the `.cmt` file, to keep only
those present in the interface.

The `-t` or `--threshold` option can be used to keep, for each function, only
hashes of nodes with either a given weight (number of subnodes + 1) or whose
weight represents a given percentage of the function. Default is `-t 0`,
meaning to keep all hashes. A 10% ceil is given with `-t 10%`.

The `--sort-hashes` option is used to sort hashes of subnodes before hashing
a node. Default is `false`.

By default, variables in lambdas are hashed. The `--dont-hash-variables` option
can be used to prevent this.

### Printing the database

```
$ inzad db show <dbfile1> <dbfile2> ...
```

Print the contents of the given inzad databases and the options used to build it
(threshold, sorting hashes or not, hashing variables or not).

### Using the database

```
$ inzad cluster <dbfile>
```

Apply clustering algorithm to partition all functions in `<dbfile>`. Beware that this
requires a lot of RAM.

```
$ inzad find -d <dbfile1> -d <dbfile> [OPTIONS] ARGS
```

The ARGS are the same as when building a database. The given `.cmt[i]` files are
analysed, functions are hashed and their main hash is looked up in the given
database files. For each function found in `.cmt[i]` files, the
functions in databases having the same main hash are listed.
This command also accepts the `--cmti`, `-t` (`-threshold`), `--sort-hashes`
and `--dont-hash-variables` options, as described above.
Addition options are:

- `--dist=0.0..1.00`: find functions similar by the given threshold
- `--mutinc=0.00..1.00,0...`: find mutually including functions by the
  given threshold and standard deviation
- `--sharing=N[%]`: find functions sharing at least N hashes or N% of
  the hashes weight

### Graphical user interface

A GUI was developed to ease experimentations. It requires [Stk](https://zoggy.frama.io/ocaml-stk/).

It is run with
```
$ inzad-gui [-d <dbfile1>] [-d <dbfile2>] [OPTIONS] ARGS
```

The ARGS are the same as when building a database. The given `.cmt[i]` files are
analysed, functions are hashed and displayed in the GUI main window. Double-clicking
on a function open a window to search for similar functions.

This command accepts the `--cmti`, `-t` (`-threshold`), `--sort-hashes`
and `--dont-hash-variables` options, as described above.

![GUI screenshot](./capture-inzad-gui.png)

