(* This file is part of asak.
 *
 * Copyright (C) 2019 IRIF / OCaml Software Foundation.
 *
 * asak is distributed under the terms of the MIT license. See the
 * included LICENSE file for details. *)

type threshold =
  | Percent of int (** A percentage threshold, a number between 0 and 100. *)
  | Hard of int (** A hard threshold. *)

type config =
  { should_sort : bool;
    (** If we sort all lists of hashes. This is useful to identify more codes,
     but can identify unfactorizable code. *)
    hash_var : bool; (** If we hash names in the AST. *)
    hash_const: bool; (** If we hash constants in the AST. *)
    apply_holes : bool ; (** If we replace arguments by holes when hashing Lapply *)
  }

(** A fingerprint *)
type fingerprint =
  | W of int * bool * Digest.t (* weight, constants hashed or not, hash *)
  | Global_field of Digest.t * string * int (* call to primitive Getglobal *)
  | Holed of Digest.t * fingerprint (* a fingerprint with a hole *)

val compare_fingerprint : fingerprint -> fingerprint -> int

module FPSet : Set.S with type elt = fingerprint
module FPMap : Map.S with type key = fingerprint

(** A type synonym for the result of the {! hash_lambda} function *)
type hash = FPSet.t

val string_of_fingerprint : fingerprint -> string
val pp_fingerprint : Format.formatter -> fingerprint -> unit
val fingerprint_digest : fingerprint -> Digest.t
val top_digest : hash -> Digest.t option
val digests : hash -> Digest.t list
val top_fingerprint : hash -> fingerprint option
val pp_top_fingerprint : Format.formatter -> hash -> unit
val top_weight : hash -> int

(** Hash a lambda expression. Usage: [hash_lambda config threshold expr].
    Two lambda expressions "of the same shape" will share the same hash. Particularly,
    constant are ignored.

    @param config A configuration for the function.

    @param threshold Used to decide which sub-AST hash is kept.

    @param expr The expression.

    @return A tuple with the main hash, and a list of hashes of sub-ASTs satisfying the threshold.
*)
val hash_lambda :
  config ->
  threshold ->
  Lambda.lambda -> hash

(** Utilities *)

val map_snd : ('a -> 'b) -> ('c * 'a) list -> ('c * 'b) list

(** Using a threshold, hash a list of lambda expressions from  {! Parse_structure.read_structure }. *)
val hash_all :
  config ->
  threshold->
  ('a * Lambda.lambda) list ->
  ('a * hash) list

(*
(** Escape hashses for printing. *)
val escape_hash :
  hash -> hash
*)
