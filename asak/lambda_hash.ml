(* This file is part of asak.
 *
 * Copyright (C) 2019 IRIF / OCaml Software Foundation.
 *
 * asak is distributed under the terms of the MIT license. See the
 * included LICENSE file for details. *)

open Lambda
open Asttypes

type threshold = Percent of int | Hard of int

type config =
  { should_sort : bool;
    hash_var : bool;
    hash_const : bool ;
    apply_holes : bool ;
  }

type fingerprint =
  | W of int * bool * Digest.t (* weight, constants hashed or not, hash *)
  | Global_field of Digest.t * string * int (* call to primitive Getglobal *)
  | Holed of Digest.t * fingerprint

let compare_fingerprint a b =
  match a, b with
  | W (w1, b1, h1), W (w2, b2, h2) ->
      (match w1 - w2 with
       | 0 ->
           (match b1, b2 with
            | true, true
            | false, false -> String.compare h1 h2
            | true, false -> -1 (* constants not hashed are last when sorting *)
            | false, true -> 1
           )
       | n -> n
      )
  | W _, _ -> 1
  | _, W _ -> -1
  | Global_field (dig1,_,_), Global_field (dig2,_,_) -> String.compare dig1 dig2
  | Global_field _, _ -> 1
  | _, Global_field _ -> - 1
  | Holed (dig1, _fp1), Holed (dig2, _fp2) ->
      String.compare dig1 dig2
      (*match compare_fingerprint fp1 fp2 with
      | 0 -> String.compare dig1 dig2
      | n -> n*)


module FP_order =
  struct
    type t = fingerprint
    let compare = compare_fingerprint
  end
module FPSet = Set.Make(FP_order)
module FPMap = Map.Make(FP_order)

type hash = FPSet.t

let rec string_of_fingerprint = function
| W (w, cst_hashed, dig) ->
    Printf.sprintf "W(%d, %b, %s)" w cst_hashed (Digest.to_hex dig)
| Global_field (dig,ident,field) ->
    Printf.sprintf "Global(%s,%d,%s)" ident field (Digest.to_hex dig)
| Holed (dig, fp) -> Printf.sprintf "Holed(%s,%s)"
    (Digest.to_hex dig) (string_of_fingerprint fp)

let pp_fingerprint ppf fp =
  let s = string_of_fingerprint fp in
  Format.pp_print_string ppf s

let top_fingerprint = FPSet.max_elt_opt
let fingerprint_digest = function
| W (_,_,d) -> d
| Global_field (d,_, _) -> d
| Holed (d,_) -> d

let top_digest hash = Option.map fingerprint_digest (top_fingerprint hash)
let digests hash = FPSet.fold
  (fun fp acc ->
    let d = fingerprint_digest fp in
    d :: acc) hash []
let top_weight =
  let rec iter = function
  | W (w,_,_) -> w
  | Global_field _ -> 1
  | Holed (_,fp) -> iter fp
  in
  fun hash ->
    match top_fingerprint hash with
    | None -> 0
    | Some fp -> iter fp


let pp_top_fingerprint ppf hash =
  match top_fingerprint hash with
  | None -> Format.pp_print_string ppf "empty hash"
  | Some (W (w,_,dig)) -> Format.fprintf ppf "%s(%d)" (Digest.to_hex dig) w
  | Some (Global_field (dig,id,field)) -> Format.fprintf ppf "%s(%s,%d)"
      (Digest.to_hex dig) id field
  | Some (Holed (_,fp)) ->
      Format.fprintf ppf "Holed(%s)" (string_of_fingerprint fp)

let h1  x = (1, Digest.string x)

(*
let hash_string_lst should_sort x xs =
  let sort xs =
    if should_sort
    then List.sort compare xs
    else xs in
  let p,lst,xs =
    List.fold_right
      (fun (u,l,x) (v,l',xs) -> u+v,(u,x)::l@l',x::xs)
      xs (0,[],[]) in
  let res =
    Digest.string @@
      String.concat "" @@
        sort @@
          if x = "" then xs else (Digest.string x::xs) in
  1+p,lst,res
*)

let hash_prim x = h1 @@
  match x with
  | Pgetglobal id -> "Pgetglobal" ^ Ident.name id
#if OCAML_VERSION >= (5, 0, 0)
  | Pfield (i, _, _) -> "Pfield" ^ string_of_int i
#else
  | Pfield i -> "Pfield" ^ string_of_int i
#endif
  | Pccall x -> "Pccall" ^ Primitive.native_name x
  | _ -> Printlambda.name_of_primitive x
(*
let hash_lst should_sort f x xs =
  let res =
    List.fold_left
      (fun acc x -> f x :: acc)
      [] xs in
  hash_string_lst should_sort x res

let hash_lst_anon should_sort f xs = hash_lst should_sort f "" xs
*)
let hash_case g f (i,x) =
  let a,b,c = f x in
  a+1,b,Digest.string (g i ^ c)

let hash_option f set =
  function
  | None -> (1, set, Digest.string "None")
  | Some x -> f set x

let hash_direction x = h1 @@
  match x with
  | Upto -> "Upto"
  | Downto -> "Downto"

let hash_meth_kind x = h1 @@
   match x with
   | Self -> "Self"
   | Public -> "Public"
   | Cached -> "Cached"

let hash_constant =
  let rec hash_cst b = function
  | Lambda.Const_base c ->
      let s = match c with
        | Asttypes.Const_int n -> string_of_int n
        | Const_char c -> String.make 1 c
        | Const_string (s,_,_) -> s
        | Const_float s -> s
        | Const_int32 n -> Int32.to_string n
        | Const_int64 n -> Int64.to_string n
        | Const_nativeint n -> Nativeint.to_string n
      in
      Buffer.add_string b s
  | Const_block (_n, l) -> List.iter (hash_cst b) l
  | Const_float_array l -> List.iter (Buffer.add_string b) l
  | Const_immstring s -> Buffer.add_string b s
  in
  fun c ->
    let b = Buffer.create 256 in
    hash_cst b c;
    h1 (Buffer.contents b)

let hash_lambda config ?(set=FPSet.empty) x =
(*  let hash_string_lst = hash_string_lst config.should_sort in
  let hash_lst_anon f = hash_lst_anon config.should_sort f in
  let hash_lst f = hash_lst config.should_sort f in*)
  let hash_var var =
    let str =
      if not config.hash_var
      then "Lvar"
      else Ident.name var
    in h1 str
  in
  let hash_hash_list ?(prefix="") hashes =
    let hashes = if config.should_sort
      then List.rev (List.sort String.compare hashes)
      else hashes
    in
    let str = prefix ^ String.concat "" hashes in
    let h = Digest.string str in
    h
  in
  let add_holes =
    let rec iter fp set prev = function
    | [] -> set
    | h :: q ->
        let dig = hash_hash_list (prev @ q) in
        let holed = Holed (dig, fp) in
        let set = FPSet.add holed set in
        iter fp set (prev @ [h]) q
    in
    fun fp set l -> iter fp set [] l
  in
  let rec hash_lambda_list ?prefix set l =
    let (w,hashes,set) =
      List.fold_left
         (fun (acc_w, acc_h, set) x ->
           let (w,set,h) = hash_lambda' set x in
           (acc_w + w, h :: acc_h, set)
        )
        (0,[],set) l
    in
    let h = hash_hash_list ?prefix hashes in
    (1+w,set,h, hashes)
  and hash_lambda' set x =
    match x with
#if OCAML_VERSION >= (5,0,0)
    | Lmutvar var
#endif
    | Lvar var ->
        (* add hash of variable to fingerprint set only if we hash variables.*)
        let (w,h) = hash_var var in
        let set =
          if config.hash_var then
            FPSet.add (W (w,config.hash_const,h)) set
          else
            set
        in
        (w,set,h)
    | Lconst const ->
       if config.hash_const
       then
          let (w,h) = hash_constant const in
          let set = FPSet.add (W (w,config.hash_const,h)) set in
          (w,set,h)
       else let (w,h) = h1 "Lconst" in (w,set,h)
    | Lapply x ->
        let (w,set,h,hashes) = hash_lambda_list ~prefix:"Lapply"
          set (x.ap_func :: x.ap_args)
        in
        let fp = W (w,config.hash_const,h) in
        let set = FPSet.add fp set in
        let set = if config.apply_holes then add_holes fp set hashes else set in
        (w,set,h)
    | Lfunction x ->
        let (w,set,h,_) = hash_lambda_list ~prefix:"Lfunction" set [x.body] in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
#if OCAML_VERSION >= (5,0,0)
    | Lmutlet (_,_,l,r)
#endif
    | Llet (_,_,_,l,r) ->
        let (w,set,h,_) = hash_lambda_list ~prefix:"Llet" set [l ; r] in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
    | Lletrec (lst,l) ->
        let (w1,set,h1,_) = hash_lambda_list set (List.map snd lst) in
        let (w2,set,h2) = hash_lambda' set l in
        let h = hash_hash_list ~prefix:"Lletrec" [h1 ; h2] in
        let w = w1 + w2 in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
#if OCAML_VERSION >= (5,0,0)
    | Lprim (Pfield (field,_, _), [Lprim (Pgetglobal ident, _, _)], _) ->
#else
    | Lprim (Pfield field, [Lprim (Pgetglobal ident, _, _)], _) ->
#endif
        let ident_name = Ident.name ident in
        let h = hash_hash_list ~prefix:"Globalfield"
          [Printf.sprintf "%s(%d)" ident_name field]
        in
        let set = FPSet.add (Global_field (h,ident_name,field)) set in
        (1,set,h)
    | Lprim (prim,lst,_) ->
        let (w1, h1) = hash_prim prim in
        let (w2,set,h2,hashes) = hash_lambda_list set lst in
        let h = hash_hash_list ~prefix:"Lprim" [h1 ; h2] in
        let w = w1 + w2 in
        let fp = W (w,config.hash_const,h) in
        let set = FPSet.add fp set in
        let set = if config.apply_holes then add_holes fp set hashes else set in
        (w,set,h)
    | Lstaticraise (_,lst) ->
        let (w,set,h,_) = hash_lambda_list ~prefix:"Lstaticraise" set lst in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
    | Lifthenelse (i,t,e) ->
        let (w,set,h,_) = hash_lambda_list ~prefix:"Lifthenelse" set [i ; t ; e] in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
    | Lsequence (l,r) ->
        let (w,set,h,_) = hash_lambda_list ~prefix:"Lsequence" set [l ; r] in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
    | Lwhile (l,r) ->
        let (w,set,h,_) = hash_lambda_list ~prefix:"Lwhile" set [l ; r] in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
    | Lifused (_,l) ->
        let (w,set,h,_) = hash_lambda_list ~prefix:"Lifused" set [l] in
        let set = FPSet.add (W (w,config.hash_const,h)) set in
        (w,set,h)
#if OCAML_VERSION >= (4, 06, 0)
    | Lswitch (l,s,_) ->
#else
    | Lswitch (l,s) ->
#endif
       let (w1,set,h1) = hash_lambda' set l in
       let hash_cases set cases =
         List.fold_right
           (fun x (acc_w,set,acc_h) ->
             let (w,set,h) = hash_case string_of_int (hash_lambda' set) x in
             (acc_w+w, set, h::acc_h)
           )
           cases (0,set,[])
       in
       let (w2,set,h2) = hash_cases set s.sw_consts in
       let (w3,set,h3) = hash_cases set s.sw_blocks in
       let (w4,set,h4) = hash_option hash_lambda' set s.sw_failaction in
       let h = hash_hash_list ~prefix:"Lswitch" (h1 :: h2 @ h3 @ [h4]) in
       let w = w1 + w2 + w3 + w4 in
       let set = FPSet.add (W (w,config.hash_const,h)) set in
       (w,set,h)
    | Lstringswitch (l,lst,opt,_) ->
       let (w1,set,h1) = hash_lambda' set l in
       let hash_cases set cases =
         List.fold_right
           (fun x (acc_w,set,acc_h) ->
             let (w,set,h) = hash_case (fun x -> x) (hash_lambda' set) x in
             (acc_w+w, set, h::acc_h)
           )
           cases (0,set,[])
       in
       let (w2,set,h2) = hash_cases set lst in
       let (w3,set,h3) = hash_option hash_lambda' set opt in
       let h = hash_hash_list ~prefix:"Lstringswitch" (h1 :: h2 @ [h3]) in
       let w = w1 + w2 + w3 in
       let set = FPSet.add (W (w,config.hash_const,h)) set in
       (w,set,h)
    | Lassign (var,l) ->
       let (w1, h1) = hash_var var in
       let (w2, set, h2) = hash_lambda' set l in
       let h = hash_hash_list ~prefix:"Lassign" [h1;h2] in
       let w = w1 + w2 in
       let set = FPSet.add (W (w,config.hash_const,h)) set in
       (w,set,h)
    | Levent (l,_) ->
       let (w,set,h,_) = hash_lambda_list ~prefix:"Levent" set [l] in
       let set = FPSet.add (W (w,config.hash_const,h)) set in
       (w,set,h)
    | Lstaticcatch (l,_,r) ->
       let (w,set,h,_) = hash_lambda_list ~prefix:"Lstaticcatch" set [l;r] in
       let set = FPSet.add (W (w,config.hash_const,h)) set in
       (w,set,h)
    | Ltrywith (l,_,r) ->
       let (w,set,h,_) = hash_lambda_list ~prefix:"Ltrywith" set [l;r] in
       let set = FPSet.add (W (w,config.hash_const,h)) set in
       (w,set,h)
    | Lfor (_,a,b,d,c) ->
       let (w1,set,h1) = hash_lambda' set a in
       let (w2,set,h2) = hash_lambda' set b in
       let (w3,h3) = hash_direction d in
       let (w4,set,h4) = hash_lambda' set c in
       let h = hash_hash_list ~prefix:"Lfor" [h1;h2;h3;h4] in
       let w = w1 + w2 + w3 + w4 in
       let set = FPSet.add (W (w,config.hash_const,h)) set in
       (w,set,h)
    | Lsend (m,a,b,xs,_) ->
       let (w1, h1) = hash_meth_kind m in
       let (w2, set, h2) = hash_lambda' set a in
       let (w3, set, h3) = hash_lambda' set b in
       let (w4, set, h4, hashes) = hash_lambda_list set xs in
       let h = hash_hash_list ~prefix:"Lsend" [h1;h2;h3;h4] in
       let w = w1 + w2 + w3 + w4 in
       let fp = W (w,config.hash_const,h) in
       let set = FPSet.add fp set in
       let set = if config.apply_holes then add_holes fp set hashes else set in
       (w,set,h)
  in
  let (w, set, h) = hash_lambda' set x in
  (w, FPSet.add (W (w,config.hash_const,h)) set)

let filter threshold main_weight set =
  let pred = function
  | Global_field _ -> true
  | Holed (_, W (w,_,_)) | W (w, _, _) ->
    (match threshold with
    | Percent i -> float_of_int w > ((float_of_int i) /. 100.) *. main_weight
    | Hard i -> w > i
    )
  | Holed _ -> true
  in
  FPSet.filter pred set

let hash_lambda config threshold l =
  let main_weight,set = hash_lambda config l in
  let set =
    if not config.hash_const then
      (
       (* rehash to add fingerprints with constants hashed *)
       let (_,set) = hash_lambda { config with hash_const = true } ~set l in
       set
      )
    else
      set
  in
  let fmain_weight = float_of_int main_weight in
  filter threshold fmain_weight set

let map_snd f xs = List.map (fun (x,y) -> x,f y) xs

let hash_all config threshold xs =
  map_snd (hash_lambda config threshold) xs

(*let escape_hash ((p,h),xs) = (p,String.escaped h),List.map (fun (p,h) -> p,String.escaped h) xs*)
