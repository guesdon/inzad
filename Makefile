SHELL := /bin/bash
#
all:
	dune build

install:
	dune build @install
	dune install

doc: all
	dune build @doc

# archive :
###########
archive:
	git archive --prefix=inzad-`git describe`/ `git describe` | gzip > /tmp/inzad-`git describe`.tar.gz

# Cleaning :
############
clean:
	dune clean

# headers :
###########
HEADFILES:=$(shell ls Makefile lib/*.ml{,i})
.PHONY: headers noheaders
headers:
	echo $(HEADFILES)
	headache -h header -c .headache_config $(HEADFILES)

noheaders:
	headache -r -c .headache_config $(HEADFILES)

